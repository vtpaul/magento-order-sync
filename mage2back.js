// ** Requires **
process.stdout.isTTY = true // This makes colors work in webstorm "oowee ..?"
process.env.TZ = 'America/New_York Time Zone'

const async     = require('async')
const colors    = require('colors').enabled = true
const request   = require('request')
const mysql     = require('mysql')
const express   = require('express')
const jsonfile  = require('jsonfile')
const fs        = require('fs')
const url       = require('url')
const moment    = require('moment-timezone')

// ** Load The Configs **
const config = require('./config.json');
const pool  = mysql.createPool(require('./' + config.mysql_config))

// ** Load Magento 'class' **
const magento = require('./magento.js')(config)

const maps = {
    payment_types: {
        authorizenet_directpost: 1,
        authnetcim: 1,
        paypal_express: 2,
    }
}

const LOOKBACK = 300

const syncOrders = (token, base_url, site, qs, nextFunc) => {
    sendHeartbeat()

    /**
     * Magento to BackOffice
     */
    console.log(`Magento ${site.toUpperCase()} to BackOffice`.yellow)
    var orderid = qs != null ? qs.orderid : null
    var metadata_file = __dirname + `/ordersync_metadata_${site}.json`
    var metadata, lastrun
    var now = new Date()
    var datetime = now.toISOString().slice(0, 19).replace('T', ' ')
    var datetime_est = moment().format().slice(0, 19).replace('T', ' ')
    console.log(moment().format())

    var connection, connection_mage
    var sql = '';
    async.waterfall([
        (cb) => {
            /**
             * get last run
             */
            metadata = getMetadata(metadata_file, cb)
        }, (metadata_result, cb) => {
            if(orderid) cb() // skip this if one-off
            else {
                /**
                 * update last run
                 */
                metadata = metadata_result
                var thepast
                if (metadata && metadata.lastrun) {
                    thepast = new Date(metadata.lastrun)
                    thepast.setSeconds(thepast.getSeconds() - LOOKBACK)
                } else {
                    metadata = {}
                    thepast = new Date()
                    thepast.setMinutes(thepast.getMinutes() - 30)
                }
                lastrun = thepast.toISOString().slice(0, 19).replace('T', ' ')
                // store new lastrun as now
                metadata.lastrun = datetime
                fs.writeFile(metadata_file, JSON.stringify(metadata), cb)
            }
        }, (cb) => {
            pool.getConnection(cb)
        }, (new_connection, cb) => {

            connection = new_connection

            connection_mage = mysql.createConnection({
                host: 'vt-magento.cbtmqjwywgwy.us-east-1.rds.amazonaws.com',
                user: 'magento',
                password: 'z2sEGz42Mc7wVNTx347ZWhjxn3NNfGUT2xJNEjHL',
                database: 'magento'
            });

            /**
             * catch missed orders for import
             */
            var missing = []
            var sql = `
                select o.entity_id
                from sales_order o
                  left join quote q on q.entity_id = o.quote_id
                where o.vt_order_id is null
                      and q.vt_order_id is null
                      and o.created_at between date_sub(now(), interval 1 day) and date_sub(now(), INTERVAL 5 MINUTE)
                      and o.status in ('processing', 'pending');`
            connection_mage.query(sql, [], (err, rows) => {
                if (err) console.error(err)
                missing = rows.map(x => { return x.entity_id })
                if(missing.length > 0)
                    console.log(`${missing.length} missed to grab: ${missing}`.yellow)
                cb(null, missing)

                /*async.eachSeries(rows, (missed, nextMissed) => {
                 console.log(`grab missed: ${missed.entity_id}...`)
                 // call this same script for one-off sync
                 request(`http://127.0.0.1:${port}/sync/orders/vt?orderid=${missed.entity_id}`, (error, response, body) => {
                 console.log(body)
                 nextMissed(error)
                 });
                 }, (err) => {
                 if (err) console.error(err)
                 // legacy connection
                 pool.getConnection(cb)
                 })*/
            })
        }, (missing, cb) => {
            /**
             * search for recently updated orders
             */
            if(orderid) console.log(`import order: ${orderid}...`)
            else console.log(`lookback: ${lastrun} (UTC)...`)

            var store_id = config[`store_id_${site}`]
            var criteria
            if(orderid) {
                criteria = `searchCriteria[filterGroups][0][filters][0][field]=entity_id&searchCriteria[filterGroups][0][filters][0][value]=${orderid}&searchCriteria[filterGroups][0][filters][0][conditionType]=in`
            } else {
                criteria = `searchCriteria[filterGroups][0][filters][0][field]=store_id&searchCriteria[filterGroups][0][filters][0][value]=${store_id}&searchCriteria[filterGroups][0][filters][0][conditionType]=eq&searchCriteria[filterGroups][1][filters][0][field]=updated_at&searchCriteria[filterGroups][1][filters][0][value]=${lastrun}&searchCriteria[filterGroups][1][filters][0][conditionType]=gt`
                var i = 1
                for(var missed of missing) {
                    criteria += `&searchCriteria[filterGroups][1][filters][${i}][field]=entity_id&searchCriteria[filterGroups][1][filters][${i}][value]=${missed}&searchCriteria[filterGroups][1][filters][${i}][conditionType]=eq`
                    i++
                }
            }

            var success = false
            var n = 0
            async.doUntil(
                (check) => {
                    console.log(`searching(${++n})...`)
                    magento.searchOrders(token, base_url, criteria, (result) => {
                        //console.log(result)
                        var t = 0
                        if(result.message) {
                            console.error(result.message)
                            t = 1000 // wait before retry
                        } else success = true

                        setTimeout(()=>{check(null, result)}, t)
                    })
                }, () => { return success },
                cb
            )
        }, (result, cb) => {
            var msg = result.total_count + ' orders to check';
            console.log(msg)

            var orders = result.items
            async.eachSeries(orders, (order, cb2) => {
                if(typeof order.status == 'undefined') {
                    console.info('no order status?')
                    cb2()
                } else {
                    console.log('----------')
                    console.log(`checking ${order.entity_id}`.magenta)

                    if(order.entity_id == 86723) {
                        console.log('skip this for now')
                        cb2()
                    } else {
                        var orderEdit = false
                        //var area = typeof order.extension_attributes.device_data != 'undefined' ? order.extension_attributes.device_data.area_name : false
                        //console.log('area:', area.blue)
                        var vto = typeof order.extension_attributes.vt_order_id != 'undefined' ? order.extension_attributes.vt_order_id : false
                        if (vto) console.log(`existing vto: ${vto}`)

                        // check vt order id
                        sql = `select 
                            o.refunds, 
                            o.payment_type, 
                            o.status, 
                            o.paid_date, 
                            o.transaction_id, 
                            o.status_date,
                            o.updated_by,
                            o.updated_date,
                            o.order_id,
                            x.*
                        from vtorder_mageorder x 
                            inner join orders o on o.order_id=x.vt_order_id 
                        where
                            x.mage_site = ? 
                            and x.mage_order_id = ?
                        order by x.vtorder_mageorder_id desc
                        limit 1;`

                        var q = connection.query(sql, [site, order.entity_id], (err, rows) => {
                            //console.log(q.sql, JSON.stringify(rows))
                            var values, status
                            if (vto || rows.length > 0) {
                                var vtorder = rows.length > 0 ? rows[0] : false
                                //console.log(vtorder)
                                if (vtorder && vtorder['legacy'] == 1) {
                                    console.log(`don't update legacy order`.blue)
                                    cb2()
                                } else if (vtorder && vtorder['reordered'] == 1 /*|| vtorder['mage_order_id'] != order.entity_id*/) {
                                    console.info('skip reordered'.blue)
                                    cb2()
                                } else {
                                    /**
                                     * it's an update
                                     */
                                    async.waterfall([(updateStep) => {
                                        /**
                                         * look for linked mage orders
                                         */
                                        connection_mage.query(`
                                            select 
                                                entity_id, 
                                                increment_id, 
                                                grand_total, 
                                                subtotal,
                                                discount_amount, 
                                                total_refunded, 
                                                tax_amount,
                                                shipping_amount,
                                                total_due,
                                                status
                                            from sales_order 
                                                where vt_order_id = ? 
                                                /*and status not in ('canceled', 'closed')*/`,
                                            [vto],
                                            updateStep
                                        )
                                    }, (rows, fields, updateStep) => {
                                        var linked_orders = rows.map(x => x.entity_id)
                                        if(rows.length > 1) console.log(`${rows.length} linked orders:`, JSON.stringify(linked_orders))
                                        var totals = {
                                            subtotal: 0,
                                            tax_amount: 0,
                                            shipping_amount: 0,
                                            discount_amount: 0,
                                            total_refunded: 0,
                                            grand_total: 0,
                                            total_paid: 0,
                                            total_due: 0,
                                            status: order.status
                                        }

                                        /**
                                         * calc order totals
                                         */
                                        if(rows.length > 0) {
                                            totals.subtotal = rows.map((x) => parseFloat(x.subtotal*100)).reduce((x, t) => x + t) / 100
                                            totals.grand_total = rows.map((x) => parseFloat(x.grand_total*100)).reduce((x, t) => x + t) / 100
                                            totals.tax_amount = rows.map((x) => parseFloat(x.tax_amount*100)).reduce((x, t) => x + t) / 100
                                            totals.shipping_amount = rows.map((x) => parseFloat(x.shipping_amount*100)).reduce((x, t) => x + t) / 100
                                            totals.discount_amount = rows.map((x) => parseFloat(x.discount_amount*100)).reduce((x, t) => x + t) / 100
                                            totals.total_refunded = rows.map((x) => parseFloat(x.total_refunded*100 || 0)).reduce((x, t) => x + t) / 100
                                            totals.total_paid = rows.map((x) => parseFloat(x.total_paid*100 || 0)).reduce((x, t) => x + t) / 100
                                            totals.total_due = rows.map((x) => parseFloat(x.total_due*100)).reduce((x, t) => x + t) / 100
                                            totals.status = rows.map((x) => x.status)
                                        }

                                        // calc status
                                        var status = rows.map((x) => x.status)
                                        if(status.indexOf('processing') > -1)
                                            totals.status = 'processing'
                                        else if(status.indexOf('pending') > -1)
                                            totals.status = 'pending'
                                        else if(status.indexOf('canceled') > -1)
                                            totals.status = 'canceled'
                                        else if(status.indexOf('closed') > -1)
                                            totals.status = 'closed'
                                        else if(status.indexOf('reversed') > -1)
                                            totals.status = 'reversed'
                                        else if(status.indexOf('complete') > -1)
                                            totals.status = 'complete'

                                        //console.log('order totals:', JSON.stringify(totals))

                                        updateStep(null, totals, rows)
                                    }, (totals, rows, updateStep) => {
                                        /**
                                         * look for magento order # in comments
                                         */
                                        async.forEachSeries(rows, (row, nextRow) => {
                                            var id = `%#${row.increment_id}%`
                                            connection.query(`select order_id, comments from orders where comments like ?`, [id], (err, rows) => {
                                                if(rows.length == 0) {
                                                    connection.query(`select comment from order_comments where comment like ?`, [id], (err, rows) => {
                                                        if (rows.length == 0) {
                                                            console.log(`add comment for ${row.increment_id}`.yellow)
                                                            connection.query(`insert into order_comments (order_id, comment, date_added, updated_by, updated_date) values (?, ?, now(), 523, now())`, [
                                                                vto,
                                                                `Linked Magento order: <a target='_blank' href='https://www.victorytailgate.com/admin_zzvddg/sales/order/view/order_id/${row.entity_id}'>#${row.increment_id}</a>`
                                                            ], (err) => {
                                                                if (err) console.log('add comment error:', err.red)
                                                                nextRow()
                                                            })
                                                        } else {
                                                            nextRow(err)
                                                        }
                                                    })
                                                } else {
                                                    nextRow(err)
                                                }
                                            })
                                        }, (err) => {
                                            if(err) console.error('Comment check error:', err.red)
                                            updateStep(null, totals)
                                        })
                                    }, (totals, updateStep) => {
                                        if (vtorder['mage_order_id'] == 0) {
                                            // it's in the middle of syncing, skip it
                                            console.info('mid sync? skip')
                                            //cb2()
                                            updateStep()
                                        } else {
                                            var ltid = typeof order.payment.last_trans_id != 'undefined' ? order.payment.last_trans_id : null

                                            /**
                                             * UPDATE ORDER
                                             */
                                            sql = `UPDATE orders
                                                SET 
                                                    name = ?,
                                                    email = ?,
                                                    phone = ?,
                                                    promo_id = ?,
                                                    billing_first_name = ?,
                                                    billing_last_name = ?,
                                                    billing_address1 = ?,
                                                    billing_address2 = ?,
                                                    billing_city = ?,
                                                    billing_state = ?,
                                                    billing_zip = ?,
                                                    shipping_first_name = ?,
                                                    shipping_last_name = ?,
                                                    shipping_address1 = ?,
                                                    shipping_address2 = ?,
                                                    shipping_city = ?,
                                                    shipping_state = ?,
                                                    shipping_zip = ?,
                                                    shipping_full_name = ?,
                                                    shipping_method = ?,
                                                    shipping_method_id = ?,
                                                    giftmsg = ?,
                                                    gift = ?,
                                                    
                                                    subtotal = ?,
                                                    shipping = ?,
                                                    tax = ?,
                                                    discount = ?,
                                                    total = ?,
                                                    refunds = ?, 
                                                    payment_type = ?, 
                                                    status = ?, 
                                                    transaction_id = ?, 
                                                    updated_by = ?, 
                                                    updated_date = ?
                                                WHERE order_id = ?
                                                LIMIT 1;`

                                            status = totals.status == 'canceled' || totals.status == 'closed' || totals.status.indexOf('reversed') > -1 ? 4 // arch/canceled
                                                : totals.status == 'complete' ? 3 // shipped
                                                : totals.total_due <= 0 ? 2 // paid
                                                : totals.status == 'processing' ? 7 // processed
                                                : totals.status == 'pending' ? 1
                                                : vtorder['status'] // leave the same
                                            //console.log(`vto status: ${vtorder['status']}, mage status: ${order.status}, target status: ${status}`)

                                            var shipping = order.extension_attributes.shipping_assignments[0].shipping
                                            if (typeof shipping.address == 'undefined')
                                                shipping = false
                                            var local = typeof order.shipping_description != 'undefined' ? (order.shipping_description.indexOf('Local') > -1) : false
                                            /*console.log(order.shipping_description)
                                             console.log(local)
                                             console.log(shipping)*/
                                            var giftmsg = ('gift_message' in order.extension_attributes) ? order.extension_attributes.gift_message.message : false

                                            values = [
                                                order.customer_firstname ? order.customer_firstname + ' ' + order.customer_lastname
                                                    : order.billing_address.firstname + ' ' + order.billing_address.lastname,
                                                order.customer_email,
                                                order.billing_address.telephone,
                                                order.coupon_code || '',
                                                order.billing_address.firstname, // billing firstname
                                                order.billing_address.lastname, // last name
                                                order.billing_address.street[0], // adr1
                                                order.billing_address.street[1] || '', // adr2
                                                order.billing_address.city, // city
                                                order.billing_address.region_code, // state
                                                order.billing_address.postcode, // zip
                                                local || !shipping ? '' : shipping.address.firstname, // ship fn
                                                local || !shipping ? '' : shipping.address.lastname, // ship ln
                                                local || !shipping ? '' : shipping.address.street[0], // ship adr1
                                                local || !shipping ? '' : shipping.address.street[1] || '', // ship adr2
                                                local || !shipping ? '' : shipping.address.city, // city
                                                local || !shipping ? '' : shipping.address.region_code, // st
                                                local || !shipping ? '' : shipping.address.postcode, // zip
                                                shipping ? shipping.address.firstname + ' ' + shipping.address.lastname : '', // ship full
                                                shipping ? order.shipping_description : '', // ship method
                                                order.shipping_description.search(/pickup/i) > -1 ? 3 : 1, // ship method id
                                                /*('marketplace_id' in order.extension_attributes) ? order.extension_attributes.marketplace_id
                                                 : order.store_id == 2 ? 10006
                                                 : 0, // site
                                                 order.increment_id || '', // site order num
                                                 ('dealer_id' in order.extension_attributes) ? order.extension_attributes.dealer_id : 0, // dealer id
                                                 order.payment.po_number || '', // customer_po*/
                                                giftmsg || '', // git msg
                                                giftmsg ? 1 : 0, // gift
                                                //ds_carrier, // ds carrier
                                                //ds_account, // ds acct
                                                //ds_zip, // ds zip

                                                ////////////

                                                totals.subtotal,
                                                totals.shipping_amount,
                                                totals.tax_amount,
                                                Math.abs(totals.discount_amount),
                                                totals.grand_total,
                                                Math.abs(totals.total_refunded),
                                                maps.payment_types[order.payment.method] || vtorder['payment_type'],
                                                status,
                                                /*status == 2 ? datetime_est
                                                 : vtorder['paid_date'] != '0000-00-00 00:00:00' ? new Date(vtorder['paid_date']).toISOString().slice(0, 19).replace('T', ' ')
                                                 : vtorder['paid_date'],*/
                                                /*vtorder['transaction_id'] ||*/ ltid,
                                                /*status != vtorder['status'] ? datetime_est
                                                 : vtorder['status_date'],*/
                                                523, // updated by todo: make mage user?
                                                datetime_est, // udpated date
                                                vto, // order_id
                                            ]

                                            // order update
                                            var q = connection.query(sql, values, (err, result) => {
                                                if (err) {
                                                    console.error('order update error:', err.message)
                                                    console.log('tried:', sql)
                                                    //cb2() // next order
                                                } else {
                                                    console.log(`updated order ${vto} / ${order.entity_id}`.green)
                                                    //console.log(values)
                                                }
                                                updateStep()
                                            })
                                        }
                                    }, (updateStep) => {
                                        //var dummyitem = false

                                        /**
                                         * TRANSACTIONS
                                         */
                                        updateTransactions(order, vto, connection, connection_mage, updateStep)

                                    }, updateStep => {
                                        /**
                                         * UPDATE ITEMS
                                         * (only if order is not canceled or the like)
                                         */
                                        var vtitems_left = []
                                        connection.query(`select * from vtorderitem_mageorderitem where vt_orderid = ? and mage_orderid = ? and mage_itemid is not null;`, [vto, order.entity_id], (err, rows) => {
                                            if (err) {
                                                console.error(err)
                                                updateStep()
                                            } else {
                                                for (var row of rows) {
                                                    vtitems_left.push(row['vt_itemid'])
                                                    //console.log(`item ${row['vt_itemid']} pushed`)
                                                }

                                                /**
                                                 * check for custom sku 4684
                                                 */
                                                let custom = false
                                                for(var x of order.items) {
                                                    if(x.sku == 4684) {
                                                        custom = true
                                                        console.log('custom sku found'.magenta)
                                                        //sendAlert(`custom on ${vto}`)
                                                        break
                                                    }
                                                }

                                                // loop mage items, store skus
                                                async.eachSeries(order.items, (mageitem, nextItem) => {
                                                    console.log(`check mageitem ${mageitem.item_id} / ${mageitem.sku} (${mageitem.qty_ordered})`.yellow)

                                                    var vtitem = false
                                                    var item_ids
                                                    async.waterfall([(checkSkuNext) => {
                                                        // only do chirins...meaning skip configurable item
                                                        if ('parent_item' in mageitem) {
                                                            console.log("\thas parent, skip".blue)
                                                            nextItem()
                                                        } else {
                                                            checkSkuNext()
                                                        }
                                                    }, (checkSkuNext) => {
                                                        // get real sku from dummy prod if needed
                                                        if (mageitem.sku == 'dummyproduct') {
                                                            console.log('skip updating dummy stuff for now'.yellow)
                                                            //cb2() // fuck it for now
                                                            //updateStep()
                                                            nextItem()
                                                        } else {
                                                            checkSkuNext()
                                                        }
                                                    }, (checkSkuNext) => {
                                                        var item_link_id = 'parent_item' in mageitem ? mageitem.parent_item.item_id : mageitem.item_id
                                                        // get vt item info
                                                        connection.query(`SET @@group_concat_max_len = 102400;
                                                        select 
                                                            i.order_id,
                                                            o.status as order_status,
                                                            group_concat(i.item_id) as item_ids,
                                                            i.price, 
                                                            sum(i.quantity) as qty_ordered,
                                                            sum(if(i.item_status = 8 and i.cancel_reason != 9, 1, 0)) as qty_canceled,
                                                            sum(if(i.item_status = 11, 1, 0)) as qty_refunded,
                                                            sum(if(i.item_status = 7, 1, 0)) as qty_shipped
                                                        from vtorderitem_mageorderitem x
                                                            inner join ordered_items i on i.item_id = x.vt_itemid
                                                            inner join orders o on o.order_id = i.order_id
                                                        where x.mage_itemid = ?`, [item_link_id], (err, res) => {
                                                            if (err) {
                                                                console.log(err)
                                                                nextItem()
                                                            } else {
                                                                //console.log('vt item:', res[1][0])
                                                                vtitem = res[1][0]
                                                                //process.exit()
                                                                checkSkuNext()
                                                            }
                                                        })
                                                    }, (checkSkuNext) => {
                                                        if (vtitem['item_ids'] == null) {
                                                            console.log('insert new...')
                                                            /**
                                                             * new insert
                                                             */
                                                            var ins_item = Object.assign({}, mageitem)
                                                            ins_item.vto = vto
                                                            ins_item.order_status = status
                                                            if(custom) ins_item.custom = true
                                                            insertVtItem(connection, connection_mage, token, ins_item, () => {
                                                                //console.log('icr')
                                                                nextItem()
                                                            })
                                                        } else {
                                                            /**
                                                             * existing, continue checks
                                                             */
                                                            item_ids = vtitem['item_ids']
                                                                .split(',')
                                                                .filter(x => x != '')
                                                                .map(x => parseInt(x))

                                                            checkSkuNext()
                                                        }
                                                    }, (checkSkuNext) => {
                                                        /**
                                                         * process refunds
                                                         */
                                                        if (vtitem['qty_refunded'] < mageitem.qty_refunded) {
                                                            console.log('refund qty...')
                                                            var limit = mageitem.qty_refunded - vtitem['qty_refunded']
                                                            var values = [[item_ids], limit]
                                                            connection.query(`update ordered_items set updated_by = 523, updated_date = now(), item_status = 11 where item_id in ? and item_status NOT IN (8, 11) limit ?`, values, (err, result) => {
                                                                if (err) console.error(err)
                                                                else if (result.affectedRows > 0) console.log(result.affectedRows + ' items refunded')
                                                                checkSkuNext()
                                                            })
                                                        } else {
                                                            checkSkuNext()
                                                        }
                                                    }, (checkSkuNext) => {
                                                        /**
                                                         * cancels
                                                         */
                                                        if (vtitem['qty_canceled'] < mageitem.qty_canceled) {
                                                            console.log('cancel qty...')
                                                            var limit = mageitem.qty_canceled - vtitem['qty_refunded'];
                                                            var values = [[item_ids], limit]
                                                            connection.query(`update ordered_items set updated_by = 523, updated_date = now(), item_status = 8 where item_id in ? and item_status NOT IN (8, 11, 7) limit ?`, values, (err, result) => {
                                                                if (err) console.error(err)
                                                                else if (result.affectedRows > 0) console.log(`${result.affectedRows} items canceled`.cyan)
                                                                checkSkuNext()
                                                            })
                                                        } else {
                                                            checkSkuNext()
                                                        }
                                                    }, (checkSkuNext) => {
                                                        /**
                                                         * removed qty (cancel with cancel_reason = 9)
                                                         */
                                                        if (vtitem['qty_ordered'] > mageitem.qty_ordered) {
                                                            console.log('remove qty', `${vtitem['qty_ordered']} vs ${mageitem.qty_ordered}`)
                                                            var limit = vtitem['qty_ordered'] - mageitem.qty_ordered
                                                            var values = [[item_ids], limit]
                                                            connection.query(`SET @uids := null; update ordered_items set updated_by = 523, updated_date = now(), item_status = 8, cancel_reason = 9 where item_id in ? and item_status != 8 and cancel_reason != 9 AND ( SELECT @uids := CONCAT_WS(',', item_id, @uids) ) limit ?; SELECT @uids as updated;`, values, (err, results) => {
                                                                if (err) {
                                                                    console.error(err)
                                                                    checkSkuNext()
                                                                } else if (results[2].length > 0) {
                                                                    //console.log(results[2])
                                                                    var updated = results[2][0].updated.toString('utf8')
                                                                    console.log(`items canceled via removal: ${updated}`.cyan)
                                                                    // remove link
                                                                    var updated_ids = updated.split(',').map(x => parseInt(x))
                                                                    connection.query(`delete from vtorderitem_mageorderitem where vt_itemid in ? limit ?`, [[updated_ids], updated_ids.length], (err, results) => {
                                                                        if (err) console.error(err)
                                                                        else console.log(' - item link removed')
                                                                        checkSkuNext()

                                                                        // add note to ordered_item
                                                                        values = updated_ids.map(x => [x, 'Qty removed from Magento order', datetime_est, 0])
                                                                        connection.query(`insert into item_comments (item_table_id, comment, date_added, updated_by) values ?`, [values])
                                                                    })
                                                                }
                                                            })
                                                        } else {
                                                            checkSkuNext()
                                                        }
                                                    }, (checkSkuNext) => {
                                                        /**
                                                         * insert new qty
                                                         */
                                                        if (vtitem['qty_ordered'] < mageitem.qty_ordered) {
                                                            var ins_item = Object.assign({}, mageitem)
                                                            ins_item.qty_ordered = mageitem.qty_ordered - vtitem['qty_ordered']

                                                            console.log(`missing ${ins_item.qty_ordered}...`)

                                                            ins_item.vto = vtitem['order_id']
                                                            ins_item.order_status = vtitem['order_status']
                                                            if(custom) ins_item.custom = true
                                                            insertVtItem(connection, connection_mage, token, ins_item, () => {
                                                                checkSkuNext()
                                                            })
                                                        } else {
                                                            checkSkuNext()
                                                        }
                                                    }, (checkSkuNext) => {
                                                        /**
                                                         * update existing non canceled/refunded
                                                         */
                                                        sql = `UPDATE ordered_items SET 
                                                                price = ?,
                                                                tax = ?,
                                                                discount = ?,
                                                                updated_date = now(), 
                                                                updated_by = 523
                                                            WHERE 
                                                                item_id IN ?
                                                                /*and item_status NOT IN (8)*/
                                                            LIMIT ?;`

                                                        var canon_item = 'parent_item' in mageitem ? mageitem.parent_item : mageitem
                                                        status = canon_item.qty_refunded == canon_item.qty_ordered ? 11 // full refund
                                                            : canon_item.qty_refunded > 0 ? 10 // partial refund
                                                            : canon_item.qty_canceled > 0 ? 8 // canceled
                                                            : canon_item.qty_shipped == canon_item.qty_ordered ? 7 // shipped
                                                            : 0 // unknown
                                                        //console.log(mageitem)
                                                        var values = [
                                                            canon_item.price,
                                                            Math.round(canon_item.tax_amount / canon_item.qty_ordered * 100) / 100,
                                                            Math.round(canon_item.discount_amount / canon_item.qty_ordered * 100) / 100,
                                                            [item_ids],
                                                            canon_item.qty_ordered,
                                                        ]
                                                        //console.log(JSON.stringify(values))
                                                        var q = connection.query(sql, values, (err, result) => {
                                                            if (err) console.error(err.red)
                                                            else if (result.affectedRows > 0) console.log(`\tupdated ${item_ids.length}`)
                                                            checkSkuNext()
                                                        })
                                                    }, (checkSkuNext) => {
                                                        for (var i of item_ids) {
                                                            vtitems_left.splice(vtitems_left.indexOf(i), 1)
                                                            //console.log(`item ${i} spliced`)
                                                        }

                                                        checkSkuNext() // all done
                                                    }], (err) => {
                                                        if (err) console.error(err)
                                                        //console.log('next item'.yellow)
                                                        nextItem()
                                                    })
                                                }, (err) => {
                                                    if (err) console.error(err)
                                                    console.log('done updating all'.green)

                                                    /**
                                                     * delete items that were completely removed from magento
                                                     */
                                                    //console.log('cancelable:', !orderEdit)
                                                    if (!orderEdit && vtitems_left.length > 0) {
                                                        console.log('orphaned items: ' + vtitems_left)
                                                        connection.query(`update ordered_items set updated_by = 523, updated_date = now(), item_status = 8, cancel_reason = 9 where item_id in ? limit ?`, [[vtitems_left], vtitems_left.length], (err, results) => {
                                                            if (err) {
                                                                console.error(err)
                                                                updateStep() // next order
                                                            } else {
                                                                console.log(`canceled missing items: ${vtitems_left.join(',')}`.cyan)
                                                                connection.query(`delete from vtorderitem_mageorderitem where vt_itemid in ? limit ?`, [[vtitems_left], vtitems_left.length], (err, results) => {
                                                                    if (err) console.error(err)
                                                                    else console.log(' - links removed')
                                                                    updateStep() // next order
                                                                })

                                                                // add note to ordered_item
                                                                values = vtitems_left.map(x => [x, 'Removed/canceled on Magento end', datetime_est, 0])
                                                                connection.query(`insert into item_comments (item_table_id, comment, date_added, updated_by) values ?`, [values])
                                                            }
                                                        })
                                                    } else {
                                                        updateStep() // next order
                                                    }
                                                }) // loop items
                                            } // link query
                                        })
                                    }], (err) => {
                                        if(err) console.error(err.red)
                                        cb2() // next order
                                    }) // vtorder check
                                }
                            } else {
                                /**
                                 * SYNC NEW ORDER
                                 */
                                /*if (order.status == 'canceled' || order.status == 'closed' || order.status.indexOf('reversed') > -1) {
                                 console.log('skip canceled insert'.blue)
                                 cb2()
                                 } else {*/
                                console.log(`insert ${order.entity_id}`.yellow)

                                // dealer info
                                var ds_carrier = ''
                                var ds_account = ''
                                var ds_zip = ''
                                //var connection_mage

                                async.waterfall([
                                    (nextStep) => {
                                        // get dealer info
                                        if (order.dealer_id) {
                                            sql = `select ds_carrier, ds_account, ds_zip from dealers where id=${order.dealer_id};`
                                            connection.query(sql, values, (rows, fields, err) => {
                                                if (err) {
                                                    console.error('dealer query error:', err.message)
                                                } else {
                                                    ds_account = rows[0]['ds_account']
                                                    ds_carrier = rows[0]['ds_carrier']
                                                    ds_zip = rows[0]['ds_zip']
                                                    nextStep()
                                                }
                                            })
                                        } else {
                                            nextStep()
                                        }
                                    }, (nextStep) => {
                                        // get marketplace info
                                        if (ds_account == '' && order.marketplace_id) {
                                            sql = `select ds_carrier, ds_account, ds_zip from marketplaces where site_id=${order.marketplace_id};`
                                            connection.query(sql, values, (rows, fields, err) => {
                                                if (err) {
                                                    console.error('marketplace query error:', err.message)
                                                } else {
                                                    ds_account = rows[0]['ds_account']
                                                    ds_carrier = rows[0]['ds_carrier']
                                                    ds_zip = rows[0]['ds_zip']
                                                    nextStep()
                                                }
                                            })
                                        } else {
                                            nextStep()
                                        }
                                    }, (nextStep) => {
                                        var rush = null;

                                        /**
                                         * BUILD ORDER INSERT
                                         */
                                        sql = `INSERT INTO orders (name, email, phone, subtotal, shipping, tax, discount, promo_id, total, refunds, date_added, payment_type, status, billing_first_name, billing_last_name, billing_address1, billing_address2, billing_city, billing_state, billing_zip, shipping_first_name, shipping_last_name, shipping_address1, shipping_address2, shipping_city, shipping_state, shipping_zip, comments, cc_type, cc_num, cc_expm, cc_expy, cc_cvv, paid_date, shipped_date, archived_date, ship_by, shipping_cost, order_cost, transaction_id, status_date, shipping_full_name, purchase_code, af_code, ordered_by, comm_paid, shipping_express, shipping_type, shipping_method, shipping_method_id, shipping_company, site, site_order_num, dealer_id, email_list_sent, email_list_sent_date, pack_slip_note, customer_po, tax_exempt_num, follow_up_flag, po_order, po_order_paid, giftmsg, alumni_id, gift, invoice_number, updated_by, updated_date, client_id, rush_charge, ship_by_status, giftmsg_url, dropship_service, dropship_account, dropship_zip) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);`

                                        //var items = order.items
                                        var transaction_id = order.payment.last_trans_id || ''
                                        var shipping = order.extension_attributes.shipping_assignments[0].shipping
                                        if (typeof shipping.address == 'undefined')
                                            shipping = false
                                        var local = typeof order.shipping_description != 'undefined' ? (order.shipping_description.indexOf('Local') > -1) : false
                                        var refund = order.total_refunded || 0
                                        var coupon = order.coupon_code || ''
                                        var giftmsg = ('gift_message' in order.extension_attributes) ? order.extension_attributes.gift_message.message : ''
                                        var comment = "Imported from Magento (<a target='_blank' href='https://www.victorytailgate.com/admin_zzvddg/sales/order/view/order_id/" + order.entity_id + "'>#" + order.increment_id + "</a>)<br><br>" + (order.customer_note || '')
                                        if ('retail_packaging' in order.extension_attributes) {
                                            if (order.extension_attributes.retail_packaging == 1)
                                                comment = comment + "<br>USE RETAIL PACKAGING"
                                        }

                                        var payment_type = maps.payment_types[order.payment.method] || 0

                                        var mpsite = ('marketplace_id' in order.extension_attributes) ? order.extension_attributes.marketplace_id
                                            : order.store_id == 2 ? 10006
                                            : 0

                                        status = order.status == 'canceled' || order.status == 'closed' || order.status.indexOf('reversed') > -1 ? 4 // arch/canceled
                                            : order.status == 'processing' ? 2 // paid
                                            : order.status == 'complete' ? 3 // shipped
                                            : 1 // pending

                                        var ctutc = new Date(order.created_at)
                                        var tsest = ctutc.setHours(ctutc.getHours() - 5)
                                        var created_at = new Date(tsest)

                                        var ponumber = 0

                                        values = [
                                            order.customer_firstname ? order.customer_firstname + ' ' + order.customer_lastname
                                                : order.billing_address.firstname + ' ' + order.billing_address.lastname,
                                            order.customer_email,
                                            order.billing_address.telephone || '',
                                            order.subtotal,
                                            order.shipping_amount,
                                            order.tax_amount,
                                            Math.abs(order.discount_amount),
                                            coupon,
                                            order.grand_total,
                                            Math.abs(refund),
                                            created_at,
                                            payment_type,
                                            status, // status
                                            order.billing_address.firstname, // billing firstname
                                            order.billing_address.lastname, // last name
                                            order.billing_address.street[0], // adr1
                                            order.billing_address.street[1] || '', // adr2
                                            order.billing_address.city, // city
                                            order.billing_address.region_code, // state
                                            order.billing_address.postcode, // zip
                                            local || !shipping ? '' : shipping.address.firstname, // ship fn
                                            local || !shipping ? '' : shipping.address.lastname || '', // ship ln
                                            local || !shipping ? '' : shipping.address.street[0], // ship adr1
                                            local || !shipping ? '' : shipping.address.street[1] || '', // ship adr2
                                            local || !shipping ? '' : shipping.address.city, // city
                                            local || !shipping ? '' : shipping.address.region_code, // st
                                            local || !shipping ? '' : shipping.address.postcode, // zip
                                            comment,
                                            '', '', 0, 0, '', // cc stuff
                                            datetime, // paid date
                                            datetime, // shipped date
                                            datetime, // archive date
                                            null, // sbd: cron will calc
                                            0, // ship cost
                                            0, // order cost?
                                            transaction_id,
                                            datetime, // status date
                                            shipping ? shipping.address.firstname + ' ' + shipping.address.lastname : '', // ship full
                                            0, // pcode
                                            null, // afcode
                                            0, // ordered by
                                            0, // comm paid
                                            0, // ship express // todo: map?
                                            90, // ship type // todo: assume res?
                                            shipping ? order.shipping_description : '', // method
                                            order.shipping_description.search(/pickup/i) > -1 ? 3 : 1, // ship method id
                                            shipping ? shipping.address.company : '', // ship company
                                            mpsite, // site
                                            order.increment_id || '', // site order num
                                            ('dealer_id' in order.extension_attributes) ? order.extension_attributes.dealer_id : 0,
                                            0, // email list sent?
                                            datetime, // email date?
                                            '', // pslip note
                                            order.payment.po_number || '', // customer_po
                                            '', // tax ex
                                            0, // flag
                                            0, // po order
                                            0, // paid
                                            giftmsg, // git msg
                                            0, // alum id
                                            giftmsg || 0, // gift
                                            '', // inv#
                                            523, // upd by
                                            datetime, // upd date
                                            null, // client
                                            rush ? rush.price : 0, // rush // todo: add something in mage for this?
                                            0, // ship by status
                                            '', // gift url
                                            ds_carrier, // ds carrier
                                            ds_account, // ds acct
                                            ds_zip // ds zip
                                        ]

                                        /**
                                         * INSERT QUERY
                                         */
                                        connection.query(sql, values, (err, result) => {
                                            if (err) {
                                                console.error('order insert error!:', err.message)
                                                console.log('tried:', err.sql)
                                                cb2() // next order
                                            } else {
                                                // get last insert id and insert into vtorder_mageorder
                                                var order_id = result.insertId
                                                msg = 'order inserted: ' + order_id
                                                console.log(msg.green)

                                                async.waterfall([
                                                    (nextQuery) => {
                                                        /**
                                                         * LINK ORDER
                                                         */
                                                        var sql = `insert into vtorder_mageorder (vt_order_id, mage_order_id, mage_site, legacy, mage_incr_id) values (?, ?, ?, 0, ?)`
                                                        connection.query(sql, [order_id, order.entity_id, site, order.increment_id], nextQuery)
                                                    }, (result, fields, nextQuery) => {
                                                        updateTransactions(order, order_id, connection, connection_mage, nextQuery)
                                                    }, (nextQuery) => {
                                                        /**
                                                         * UPDATE MAGE ORDER
                                                         */
                                                        connection_mage.query('update sales_order set vt_order_id = ?, marketplace_id = ? where entity_id = ? limit 1;', [order_id, mpsite, order.entity_id], (err) => {
                                                            if (err) console.error(err)
                                                            nextQuery(err)
                                                        })
                                                    }, (nextQuery) => {
                                                        /**
                                                         * UPDATE MAGE QUOTE
                                                         */
                                                        connection_mage.query('update quote set vt_order_id = ? where entity_id = ? limit 1;', [order_id, order.quote_id], (err) => {
                                                            if (err) console.error(err)
                                                            nextQuery(err)
                                                        })
                                                    }, (nextQuery) => {
                                                        /**
                                                         * check for custom sku 4684
                                                         */
                                                        let custom = false
                                                        for(var x of order.items) {
                                                            if(x.sku == 4684) {
                                                                custom = true
                                                                console.log('custom sku found'.magenta)
                                                                //sendAlert(`custom on ${order_id}`)
                                                                break
                                                            }
                                                        }

                                                        let groupIds = {}
                                                        async.eachSeries(order.items, (item, nextItem) => {
                                                            if (item.product_type == 'configurable') {
                                                                nextItem()
                                                                return
                                                            }

                                                            /** get uuid from quote */
                                                            let uuid
                                                            connection_mage.query(`select uuid from quote_item where item_id = ?`, [item.quote_item_id], (err, result) => {
                                                                uuid = result.length > 0 ? result[0].uuid : null; console.log('mage gid:', uuid)
                                                                //insertStep(err, mageitem)
                                                                /** get next avail group id */
                                                                connection.query(`select ifnull(max(cast(group_id as unsigned))+1, 1) as gid from ordered_items where order_id = ? limit 1;`, [order_id], (err, result) => {
                                                                    if(err) console.log(err)

                                                                    let gidNext = result.length > 0 ? result[0].gid : 1
                                                                    let gid = uuid ? groupIds[uuid] || gidNext : gidNext; console.log('vt gid:'.magenta, gid)
                                                                    if(uuid) groupIds[uuid] = gid; //console.log(groupIds)

                                                                    item.gid = gid
                                                                    item.vto = order_id
                                                                    item.order_status = status
                                                                    if (custom) item.custom = true
                                                                    insertVtItem(connection, connection_mage, token, item, () => {
                                                                        nextItem()
                                                                    })
                                                                })
                                                            })


                                                        }, (err) => {
                                                            nextQuery(err)
                                                        })
                                                    }
                                                ], (err) => {
                                                    if(err) console.error(err.red)
                                                    cb2() // next order
                                                })
                                            } // no error
                                        })
                                    }
                                ])
                                //}
                            }
                        })

                    }
                }
            }, (err) => {
                if(err) {
                    console.error('error on order '+order.order_id, err.message)
                } else {
                    //console.log("------------".yellow)
                    console.log("--- done ---".yellow)
                    //console.log("------------".yellow)
                }
                cb() // wrap up
            }) // each order
        }, (err) => {
            //if(err) console.error(err)

            // done...
            if(orderid) {
                //console.log('we out?')
                //console.log(nextFunc)
                // individual order sync
                nextFunc() // callback
            } else if(nextFunc) {
                setTimeout(() => { nextFunc(token, site, false, false, syncOrders) }, 30000) // addTracking
            }

            connection.release()
            connection_mage.end(err => {
                if(err) console.error(err)
            })
        }
    ], (err) => {
        if(err) {
            console.error(err)
            connection.release()
        }
    })
}

const addTracking = (token, site, orderid, dry, nextFunc) => {
    sendHeartbeat()

    //var dry = false // for debugging, no actual inserts

    var base_url = config[`magento_base_url_${site}`]
    var site_ids = config[`sites_${site}`]

    console.log(`Tracking Postback ${site}`.yellow)

    /*var logfile = 'tracking.log'
     var metadata_file = __dirname + `/tracking_metadata_${site}.json`
     var metadata, lookback*/

    var now = moment()
    console.log(now.format())

    var connection
    async.waterfall([(cb) => {
        /**
         * get last run

         metadata = getMetadata(metadata_file, cb)
         }, (metadata_result, cb) => {
        metadata = metadata_result
        if (metadata && metadata.lastrun) {
            var lastrun = moment(metadata.lastrun)
            lookback = lastrun.seconds(lastrun.seconds()-30)
        } else {
            metadata = {}
            lookback = moment(now).minutes(now.minutes()-30)
        }
        lookback = lookback.format().slice(0, 19).replace('T', ' ')

        // store new lastrun as now
        metadata.lastrun = now.format().slice(0, 19).replace('T', ' ')
        cb()
    }, (cb) => {*/
        pool.getConnection(cb)
    }, (new_connection, cb) => {
        connection = new_connection
        if(orderid)
            console.log(`search VTO ${orderid}`)
        //else console.log(`lookback: ${lookback} (EST)`)
        //var search = orderid ? `o.order_id=${orderid}` : `ot.date_added > "${lookback}"`
        var sql_get_tracking = `
            select
              it.ordered_item_id,
              i.product_id,
              i.product_type,
              o.order_id,
              xi.mage_orderid,
              ot.tracking_num,
              lower(ot.carrier) as carrier,
              xi.mage_itemid
            from ordered_item_tracking it
              inner join ordered_items i on i.item_id=it.ordered_item_id
              inner join orders o on o.order_id=i.order_id
              inner join order_tracking ot on ot.track_id=it.track_id
              inner join vtorderitem_mageorderitem xi on xi.vt_itemid=it.ordered_item_id
            where
              ot.date_added > "2018-06-04 19:00:00"
              and xi.mage_shipped = 0
              and xi.mage_itemid is not null
              and xi.mage_orderid > 0
              and o.site IN (?)
            order by o.order_id asc;`
        var q = connection.query(sql_get_tracking, [site_ids], cb)
        //console.log(q.sql);
    }, (rows, fields, cb) => {
        var i = 0
        console.log(rows.length+' items to ship')
        var skip = null
        var mage_order = null
        async.eachSeries(rows, (item, nextItem) => {
            if(i==200) {
                cb('limit break')
            } else {
                i++
                if (skip == item['mage_orderid']) {
                    //console.log(`skip ${skip}`)
                    nextItem()
                } else {
                    console.log(`${i}/${rows.length}) item: ${item['mage_itemid']} with tracking ${item['tracking_num']} to ${item['mage_orderid']} / ${item['order_id']}`)
                    skip = null
                    async.waterfall([(itemStep) => {
                        setTimeout(itemStep, 400)
                    }, (itemStep) => {
                        // get mage order so we can get items in next step
                        if (mage_order == null || item['mage_orderid'] != mage_order.entity_id) {
                            magento.getOrder(token, base_url, item['mage_orderid'], (result) => {
                                if (result.status == 'complete' || result.status == 'closed') {
                                    var q = connection.query(`update vtorderitem_mageorderitem set mage_shipped = 1 where vt_orderid = ?;`, [item['order_id']], (err, result) => {
                                        //console.log(q.sql, result.affectedRows)
                                        skip = item['mage_orderid']
                                        itemStep(`order already complete/closed`)
                                    })
                                } else {
                                    mage_order = result
                                    itemStep()
                                }
                            })
                        } else {
                            // we already got it
                            itemStep()
                        }
                    }, (itemStep) => {
                        var compare = false
                        magento.getOrderItem(token, base_url, item['mage_itemid'], (result) => {
                            //console.log(`ordered: ${result.qty_ordered} shipped: ${result.qty_shipped}`)
                            if('message' in result) {
                                console.log(`delete from vtorderitem_mageorderitem where mage_itemid = ? limit 1;`)
                                connection.query(`delete from vtorderitem_mageorderitem where mage_itemid = ? limit 1;`, [item['mage_itemid']], (err) => {
                                    if (err) console.error('del err:', err)
                                    else console.log('item  not found, link deleted'.yellow)
                                    itemStep(`item not found`)
                                })
                            } else if (result.qty_ordered > 0 && result.qty_ordered == result.qty_shipped + result.qty_refunded + result.qty_returned + result.qty_canceled) {
                                connection.query(`update vtorderitem_mageorderitem set mage_shipped = 1 where mage_itemid = ?;`, [item['mage_itemid']], (err) => {
                                    if (err) console.error('upd err:', err)
                                    else console.log(`marking shipped`.yellow)
                                    itemStep(`item already shipped`)
                                })
                            } else if (result.sku == 'dummyproduct') {
                                console.log('dummy prod')
                                compare = result.product_option.extension_attributes.custom_options[0].option_value
                                itemStep(null, result)
                            } else {
                                itemStep(null, result)
                            }
                        })
                    }, (mageitem, itemStep) => {
                        var qty = config.board_types.indexOf(item['product_type']) > -1 ? 2 : 1
                        var data = {}
                        if (mageitem.sku == 'dummyproduct' && compare != item['product_id']) {
                            console.log(`no match? ${compare} / ${item['product_id']}`)
                            itemStep()
                        } else {
                            //var oid = mage_item.parent_item_id || mage_item.item_id
                            data = {
                                id: item['mage_orderid'],
                                items: [{'orderItemId': item['mage_itemid'], qty: qty}],
                                tracking: [{
                                    'trackNumber': item['tracking_num'],
                                    'carrierCode': item['carrier']
                                }]
                            }
                            //console.log(`posting ${JSON.stringify(data)}`.yellow)
                            if (dry) {
                                console.log('debug mode, skipping post)')
                                itemStep()
                            } else {
                                magento.addTracking(token, base_url, data, (result) => {
                                    if (result.message) {
                                        console.error(`${result.message}`)
                                        itemStep()
                                    } else {
                                        console.log(`shipped item ${result} for order ${item['mage_orderid']} / ${item['order_id']}`.green)
                                        connection.query(`update vtorderitem_mageorderitem set mage_shipped = 1 where vt_itemid = ? limit 1;`, [result], (err) => {
                                            if (err) console.error(err)
                                            itemStep()
                                        })
                                    }
                                })
                            }
                        }
                    }], (err) => {
                        if (err) console.log(err.red)
                        nextItem()
                    })
                }
            }
        }, () => { cb(null) })
    }, (cb) => {
        //if(msg) console.log(`msg: ${msg}`)
        //console.log("------------".yellow)
        console.log("--- done ---".yellow)
        //console.log("------------".yellow)
        cb(null)
        /*fs.writeFile(metadata_file, JSON.stringify(metadata), (err) => {
         if (err) console.error(err)
         connection.release()
         })*/
    }], (err) => {
        if(err)
            console.error(err)

        connection.release()

        if(nextFunc) {
            setTimeout(() => {nextFunc(token, base_url, site, null, addTracking)},30000) // syncOrders
        }
    })
}

const getMetadata = (file, callback) => {
    var metadata = {}
    jsonfile.readFile(file, (err, obj) => {
        if (err && err.errno!=-2) {
            console.error(err)
        } else {
            metadata = obj
            callback(null, metadata)
        }
    })
}

const sendHeartbeat = () => {
    var name = site=='k12' ? 'OrderSyncK12' : 'OrderSync'
    request({
        method: 'GET',
        url: 'https://api.opsgenie.com/v2/heartbeats/'+name+'/ping',
        headers: { 'content-type': 'application/json', 'authorization': 'GenieKey c8715792-5166-480b-8983-a21976478dda' },
        json: true
    })
}

const sendAlert = (msg) => {
    var body = {
        "message": 'Mage order sync error',
        "description": msg,
        "teams": [{"name": "Web"}],
        "tags": ["Magento"],
        "priority": "P3"
    }
    request({
        method: 'POST',
        url: 'https://api.opsgenie.com/v2/alerts',
        headers: { 'content-type': 'application/json', 'authorization': 'GenieKey ffcb3c34-e6d0-47c3-b1a6-a78659e530bf' },
        body: JSON.stringify(body)
    }, (err, r) => {
        if(err) console.error(err)
    })
}

const insertVtItem = (connection, connection_mage, token, item, callback) => {
    console.log(`insert ${item.qty_ordered} item(s)...`)
    if(!item.sku) {
        console.error('bad sku, skipping'.red)
        callback()
        return
    }

    let custom = 'custom' in item

    var custom_info = false
    var itemids = []
    var mageitem

    async.waterfall([
        (insertStep) => {
            connection.query('select custom_text from products where id = ?', [item.sku], (err, result) => {
                insertStep(null, result[0])
            })
        }, (vtprod, insertStep) => {
            /**
             * get magento item
             */
            var mage_item_id = 'parent_item' in item ? item.parent_item.item_id : item.item_id
            magento.getOrderItem(token, base_url, mage_item_id, (orderitem) => {
                mageitem = orderitem
                if(item.qty_ordered) mageitem.qty_ordered = item.qty_ordered
                mageitem.item_status = mageitem.qty_refunded == mageitem.qty_ordered ? 11 // full refund
                    : mageitem.qty_refunded > 0 ? 10 // partial refund
                    : mageitem.qty_canceled > 0 ? 8 // canceled
                    : mageitem.qty_shipped == mageitem.qty_ordered ? 7 // shipped
                    : 0 // unknown

                if(/*(vtprod && vtprod.custom_text == 1) ||*/ item.name.match(/custom/i))
                    mageitem.item_status = 19 // pending customization

                if(typeof orderitem.product_option != 'undefined') {
                    custom_info = ''
                    var options

                    /**
                     * check for giftcard
                     */
                    options = orderitem.product_option.extension_attributes.giftcard_item_option
                    if(typeof options != 'undefined') {
                        /** LOOKS LIKE:
                         "giftcard_item_option": {
                            "giftcard_amount": "200",
                            "giftcard_sender_name": "Karen Northcutt",
                            "giftcard_recipient_name": "Sarah Coll",
                            "giftcard_sender_email": "northcutt.karen@gmail.com",
                            "giftcard_recipient_email": "sarah@mmc3.com"
                        }*/
                        custom_info += JSON.stringify(options)
                    }

                    /**
                     * check custom text
                     */
                    options = orderitem.product_option.extension_attributes.custom_options
                    if(typeof options != 'undefined') {
                        console.log(`customization found`.magenta)

                        mageitem.item_status = 19 // pending customization

                        // set order to pending if needed
                        //connection.query("update orders set status = 1 where order_id = ? and status != 1 limit 1;", [item.order_id])

                        custom_info += `</span><br><br><span>Customization:`
                        //for (var option of options) {
                        async.eachOfSeries(options, (option, okey, nextOption) => {
                            //console.log(`${okey} option: ${JSON.stringify(option)}`)
                            if (option.option_value.indexOf('image') > -1) {
                                var vals = option.option_value.split(',')
                                /**
                                 * LOOKS LIKE:
                                 * [
                                 * image/png,
                                 * Nascar.png,
                                 * custom_options/quote/N/a/a38967c57daaafde298907b3670fb83f.png,
                                 * custom_options/order/N/a/a38967c57daaafde298907b3670fb83f.png,
                                 * /app/magento2/pub/media/custom_options/quote/N/a/a38967c57daaafde298907b3670fb83f.png,
                                 * 22130,
                                 * 107,
                                 * 59,
                                 * a38967c57daaafde2989
                                 * ]
                                 */
                                custom_info += `Uploaded file: https://www.victorytailgate.com/media/${vals[3]}`
                                nextOption()
                            } else {
                                // get real option value
                                async.waterfall([
                                    (go) => {
                                        var url = base_url + `/V1/products/${mageitem.sku}/options/${option.option_id}`
                                        var reqdata = {
                                            method: 'GET',
                                            url: url,
                                            headers: {
                                                'cache-control': 'no-cache',
                                                authorization: 'Bearer ' + token
                                            }, json: true
                                        }
                                        //console.log(url)
                                        request(reqdata, function (error, response, body) {
                                            if (error) console.error(error)
                                            if(response.statusCode != 200) {
                                                console.error(`option not found?`)
                                                custom_info += `<br>field ${okey + 1}: `
                                            } else {
                                                custom_info += `<br>${body.title}: `
                                            }

                                            if (body.values != undefined) {
                                                for (var val of body.values) {
                                                    if (option.option_value == val.option_type_id) {
                                                        custom_info += val.title
                                                        break
                                                    }
                                                }
                                            } else {
                                                custom_info += option.option_value
                                            }

                                            go()
                                        })
                                    }, (go) => {
                                        //console.log('next opt')
                                        go()
                                    }
                                ], (err) => {
                                    if(err) console.error(`err1: ${err}`)
                                    nextOption()
                                })
                            }
                        }, (err) => {
                            if(err) console.error(`err2: ${err}`)
                            insertStep(null, mageitem)
                        })
                    } else {
                        insertStep(null, mageitem)
                    }
                } else {
                    insertStep(null, mageitem)
                }
            })
        }, (mageitem, done) => {
            // get product data to check product type...
            magento.getProduct(token, base_url, mageitem.sku, (product) => {
                /**
                 * get slots
                 * $this->db->where("product_id",$id);
                 * $slots = $this->db->get("products_slots",$quantity,array("slot_id","slot_num"));
                 */
                var slots = []
                var slot_ids = []
                connection.query(`select slot_id, slot_num, quantity from products_slots where product_id = ? limit ?`, [mageitem.sku, mageitem.qty_ordered], (err, rows, field) => {
                    if (err) console.error(err)

                    if (rows) {
                        for (var row of rows) {
                            for(var x = 0; x < row['quantity']; x++) {
                                slots.push(row['slot_num'])
                                slot_ids.push(row['slot_id'])
                            }
                        }
                    }

                    var info = ''
                    var board = false
                    for (var attr of product.custom_attributes) {
                        if (attr.attribute_code == 'product_type'
                            && (attr.value == 1346
                                || attr.value == 1582
                                || attr.value == 1579
                                || attr.value == 1349 // unfin
                                || attr.value == 1350
                                || attr.value == 1347
                                || attr.value == 1348 // primered
                                || attr.value == 1387 // custom
                                // use parent prod 1623 ??
                            )) {
                            // board types
                            //info = 'ONE BOARD'
                            board = true
                        }
                    }

                    /**
                     * loop for splitting
                     */
                    var loopobj = []
                    for (var i = 0; i < mageitem.qty_ordered; i++)
                        loopobj.push('loop')
                    var slot = null
                    async.eachOfSeries(loopobj, (v, i, cb) => {
                        if(board)
                            console.log(`board ${(i % 2) + 1}`)

                        //if(!board || (board && (i % 2 == 0)))
                        slot = slots.length > 0 ? slots.shift() : ''

                        var thisinfo = ''
                        if (board && !slot) {
                            // ALTERNATE A/B
                            thisinfo += (i % 2 == 0) ? 'ONE BOARD PRINT A' : 'ONE BOARD PRINT B'
                        }

                        if(board && slot)
                            console.log(`in-stock board, ${slots.length} slots`.cyan)

                        if(custom_info) {
                            thisinfo += `${custom_info}<br>`
                        }

                        var values = [
                            item.vto,
                            mageitem.sku,
                            item.name,
                            '', // style
                            mageitem.price,
                            mageitem.created_at,
                            1,
                            slot,
                            0, // todo: category needed?
                            Math.round(mageitem.amount_refunded / mageitem.qty_ordered * 100) / 100,
                            custom ? 19 : mageitem.item_status,
                            thisinfo,
                            Math.round(mageitem.tax_amount / mageitem.qty_ordered * 100) / 100,
                            Math.round(mageitem.discount_amount / mageitem.qty_ordered * 100) / 100,
                            item.gid, //mageitem.uuid,
                            mageitem.sku
                        ]

                        if(mageitem.sku == 'giftcard') {
                            // use vt generic gift card sku
                            values[1] = values[15] = 22229
                            values[10] = 7 // mark gift card shipped
                        }

                        var sql = `insert into ordered_items (order_id, product_id, name, style, price, date_added, quantity, slot_nums, category, refund_amt, updated_date, cwg_customer_price, updated_by, item_status, product_type, info, tax, discount, group_id) select ?,?,?,?,?,?,?,?,?,?,now(),0,523,?,product_type,?,?,?,? from products where id=?`

                        // try insert a few times
                        var tries = 0
                        var success = false
                        async.whilst(
                            () => { return tries < 3 && success == false },
                            (retry) => {
                                tries++
                                if(tries > 1)
                                    console.log(`trying insert(${tries})...`)

                                connection.query(sql, values, (err, result) => {
                                    if (err) {
                                        console.error(`error, retrying: ${err.message}`)
                                        retry(null, tries)
                                    } else {
                                        success = true
                                        var vtitemid = result.insertId

                                        /**
                                         * status history
                                         */
                                        connection.query(`insert into ordered_item_status_history (item_id, original_status, new_status, updated_at, updated_by, notes) values (?, null, ?, now(), ?, ?)`, [vtitemid, mageitem.item_status, 523, 'Initial sync'], (err) => {
                                            if(err) console.log('status history error:'.red, err)
                                        })

                                        connection.query(`insert into vtorderitem_mageorderitem (sku, mage_site, vt_orderid, vt_itemid, mage_itemid, mage_orderid) values ?`, [[[mageitem.sku, site, item.vto, result.insertId, mageitem.item_id, mageitem.order_id]]], (err) => {
                                            if (err) {
                                                console.error(err, values)
                                                cb('link item error')
                                            } else {
                                                console.log(`\tok: ${vtitemid}`.green)

                                                /**
                                                 * REMOVE SLOTS / STOCK
                                                 */
                                                if (slot && mageitem.order_status != 4 && mageitem.item_status != 8) {
                                                    connection.query(`update products_slots set quantity = quantity - 1 where slot_num = ? and product_id = ? and quantity > 0 limit 1;`, [slot, mageitem.sku], (err) => {
                                                        if (err) console.error(err)
                                                        else console.info('slot ' + slot + ' decremented for ' + mageitem.sku)
                                                        cb()
                                                    })
                                                } else cb()
                                            }
                                        })
                                    }
                                })
                            }, (err, n) => {
                                cb(`tried 3 times, aborting`)
                                sendAlert(`Insert item failed on ${mageitem.order_id} / ${item.vto}`)
                            }
                        )
                    }, (err) => {
                        if(err) console.error(err)
                        callback(itemids)
                        done()

                        /**
                         * REMOVE SLOTS / STOCK
                         if (item.order_status != 4 && item.item_status != 8) {
                            if (slot_ids.length > 0) {
                                connection.query(`delete from products_slots where slot_id in (?) limit ?`, [slot_ids.join(','), slot_ids.length], (err, result) => {
                                    if (err) console.error(err)
                                    else console.info('slots removed for ' + item.sku)
                                })
                            }

                            })
                         }*/
                        connection.query(`update products set stock = stock - ? where id = ? and stock > 0 limit 1`, [mageitem.qty_ordered, mageitem.sku], (err, result) => {
                            //if (err) console.error(err)
                        })
                    })
                })
            })
        }
    ], (err) => {
        if(err) console.error(err)
    })
}

const updateTransactions = (order, vto, connection, connection_mage, callback) => {
    //console.log('update transactions...')
    async.waterfall([
        (nextCheck) => {
            /**
             * TRANSACTIONS
             */
                //var sql = `select * from sales_payment_transaction where order_id = ?`
            var sql = `select
              t.txn_type as type,
              p.method,
              if(t.txn_type = 'refund', p.amount_refunded, p.amount_paid) as amount,
              t.txn_id as payment_id,
              t.transaction_id as magento_transaction_id,
              t.created_at as date_added
            from sales_order_payment p
              left join sales_payment_transaction t on t.payment_id = p.entity_id
            where t.order_id = ?`
            var q = connection_mage.query(sql, [order.entity_id], (err, rows) => {
                //console.log(rows)
                //console.log(q.sql)
                if(rows.length > 0) {
                    /**
                     sample gateway response transaction table:
                     {"raw_details_info":{"response_code":1,"response_subcode":"","response_reason_code":0,"response_reason_text":"","approval_code":"182103","auth_code":"182103","avs_result_code":"Y","transaction_id":"61192020689","reference_transaction_id":"","invoice_number":"000115928","description":"","amount":"234.98","method":"CC","transaction_type":"auth_capture","customer_id":"","md5_hash":"BF45D66ACE3E78E8F7163867CF060959","card_code_response_code":"M","cavv_response_code":"","acc_number":"XXXX3565","card_type":"Visa","split_tender_id":"","requested_amount":"","balance_on_card":"","profile_id":"1928601037","payment_id":"1941714219","is_fraud":false,"is_error":false}}
                     */
                    /**
                     * payment table authnet
                     * {"save":0,"acceptjs_key":null,"acceptjs_value":null,"method_title":"Credit Card Payment","response_code":1,"response_subcode":"","response_reason_code":0,"response_reason_text":"","approval_code":"202588","auth_code":"202588","avs_result_code":"N","transaction_id":"61196305453","reference_transaction_id":"","invoice_number":"000117096","description":"","amount":"254.98","method":"CC","transaction_type":"auth_capture","customer_id":"63462","md5_hash":"0C510A286C0332DB8DE4F97A560236FC","card_code_response_code":"M","cavv_response_code":"","acc_number":"XXXX1012","card_type":"AmericanExpress","split_tender_id":"","requested_amount":"","balance_on_card":"","profile_id":"1928870073","payment_id":"1942004944","is_fraud":false,"is_error":false}
                     */
                    /**
                     * payment table paypal
                     * {"paypal_express_checkout_shipping_overridden":1,"paypal_express_checkout_shipping_method":"","paypal_payer_id":"4LYL7UFN5UJQ6","paypal_payer_email":"dwfloridaartist@gmail.com","paypal_payer_status":"unverified","paypal_address_status":"Confirmed","paypal_correlation_id":"c8988982c3b63","paypal_express_checkout_payer_id":"4LYL7UFN5UJQ6","paypal_express_checkout_token":"EC-4MB80560NU1646718","method_title":"PayPal Express Checkout","paypal_express_checkout_redirect_required":null,"paypal_protection_eligibility":"Eligible","paypal_payment_status":"completed","paypal_pending_reason":"None"}
                     */
                    var values = rows.map(x => {
                        //var info = x.additional_information != null && x.additional_information != 'null' ? JSON.parse(x.additional_information).raw_details_info : null
                        //console.log(info, x.additional_information)
                        return [
                            vto,
                            x.type,
                            x.method,
                            x.amount,
                            x.payment_id,
                            x.magento_transaction_id,
                            x.date_added
                        ]
                    })
                    console.log('transactions...')
                    async.eachSeries(values, (value, nextInsert) => {
                        var q = connection.query(`insert into transactions (order_id, type, method, amount, payment_id, magento_transaction_id, date_added) values (?)`, [value], (err) => {
                            //console.log(q.sql, err)
                            if (err) {
                                if(err.code != 'ER_DUP_ENTRY') console.error(err.sqlMessage.red)
                            } else console.log(`\t${value[1]} inserted`.green)
                            nextInsert()
                        })
                    }, nextCheck)
                } else nextCheck()
            })
        }, (nextCheck) => {
            /**
             * CHECK STORE CREDIT
             */
            if(typeof order.extension_attributes.customer_balance_amount != 'undefined') {
                var amt = order.extension_attributes.customer_balance_amount
                var sql = `insert into transactions (order_id, type, method, amount, payment_id, date_added) values ?`
                //var date = new Date().toISOString().slice(0, 19).replace('T', ' ')
                var values = [[
                    vto,
                    'customer balance',
                    'store credit',
                    amt,
                    `SC-${order.quote_id}`,
                    order.created_at
                ]]
                connection.query(sql, [values], (err) => {
                    if (err && err.code != 'ER_DUP_ENTRY') console.error(err.sqlMessage.red)
                    nextCheck()
                })
            } else {
                nextCheck()
            }
        }, (nextCheck) => {
            /**
             * CHECK GIFT CARD
             */
            if (order.extension_attributes.gift_cards.length > 0) {
                var sql = `insert into transactions (order_id, type, method, amount, payment_id, date_added, note) values ?`
                var values = order.extension_attributes.gift_cards.map(x => {
                    return [
                        vto,
                        'gift card',
                        'gift card',
                        x.amount,
                        `GC-${order.quote_id}-${x.code}`,
                        order.created_at,
                        x.code
                    ]
                })
                connection.query(sql, [values], (err) => {
                    if (err && err.code != 'ER_DUP_ENTRY') console.error(err.sqlMessage.red)
                    nextCheck()
                })
            } else nextCheck()
        }
    ], callback)
}

// Express Stuff
const app = express()

var processed = []
var cur_cust = []
var cur_prod = []

var port = process.env.PORT || 3100
var site = process.argv[2] || 'none'
var site_ids
var base_url

// attach jquery to express
app.get('/jquery.min.js', (req, res) => {
    res.sendFile(__dirname + '/node_modules/jquery/dist/jquery.min.js')
})

app.get('/sync/orders/:site', (req, res) => {
    site = req.params.site
    base_url = config[`magento_base_url_${site}`]
    var token = config[`magento_api_Access_Token_${site}`]
    var qs = url.parse(req.url, true).query
    if(qs.orderid) {
        console.log('remote order sync called')
        syncOrders(token, base_url, site, qs, () => {
            res.end("remote order sync done\n")
        });
    }
})

app.get('/tracking/:site', (req, res) => {
    site = req.params.site
    var token = config[`magento_api_Access_Token_${site}`]
    var qs = url.parse(req.url, true).query
//if(qs.orderid)
    addTracking(token, site, qs.orderid, qs.dry, null)

    res.end()
})

app.use(express.static('public'))

// Startup Stuff
async.waterfall([(cb) => {
    port = 3101

    app.listen(port, "0.0.0.0", cb)
}], err => {
    if(err){
        console.log('Startup failed: ' + err)
        return
    }
    moment.tz.setDefault('America/New_York')
    var msg = 'Startup successful! ('+port+') '+"\n"+ moment().format()
    console.log(msg.white)

    base_url = config[`magento_base_url_${site}`]
    var token = config[`magento_api_Access_Token_${site}`]
    /*var qs = url.parse(req.url, true).query*/
    site_ids = config[`sites_${site}`]

    if(site!='none') {
        syncOrders(token, base_url, site, null, addTracking)
    }
})
