module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [
    // VT
    /*{
      name      : 'osvt',
      script    : 'ordersync.js',
      args      : 'vt',
      log_date_format : "MM-DD HH:mm:ss"
    },*/
    {
      name      : 'm2b',
      script    : 'mage2back.js',
      args      : 'vt',
      log_date_format : "MM-DD HH:mm:ss"
    },
    {
      name      : 'b2m',
      script    : 'back2mage.js',
      args      : 'vt',
      log_date_format : "MM-DD HH:mm:ss"
    },
  ]
};
