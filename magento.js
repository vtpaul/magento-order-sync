const request   = require('request')
const async     = require('async')
const md5       = require('js-md5')
const fs        = require('fs')

var self  = {
    config: {},

    addTracking: (token, base_url, data, callback) => {
    var url = base_url + self.config.magento_ship_url.replace('{id}', data.id)
    var postbody = {
        'items': data.items,
        'tracks': data.tracking,
        'notify': false,
        'appendComment': true,
        'comment': {'comment': 'Via sync script', 'isVisibleOnFront': 0}
    }
    request({
        method: 'POST',
        url: url,
        auth: {'bearer': token},
        body: postbody,
        json: true,
        gzip: true
    }, (err, r, body) => {
    if (err) callback({message:err})
    if (r.statusCode != 200) {
        console.error(body)
        callback({message: `error (${r.statusCode})`, code: r.statusCode})
    } else callback(body)
})
},

searchOrders: (token, base_url, criteria, cb) => {
    var url = base_url + '/V1/orders?' + criteria
    request({
        method: 'GET',
        url: url,
        auth: { 'bearer': token },
        json: true
    }, (e, r, body) => {
        if(e || r.statusCode!=200) {
        if(e) console.error(e)
        else console.error('searchOrders error ('+r.statusCode+'):', body, 'tried:', url)
        cb({message:'order search error'})
    } else {
        cb(body)
    }
})
},

getOrder: (token, base_url, order_id, callback) => {
    request({
        method: 'GET',
        url: base_url + '/V1/orders/' + order_id,
        auth: {'bearer': token},
        json: true
    }, (e, r, body) => {
        if (e || r.statusCode != 200) {
        console.error(r.statusCode + ') getOrder error: ' + e)
        console.error('tried '+base_url + '/V1/orders/' + order_id)
    }
    if(body.message) console.error('getOrder error: ' + r.statusCode + ') ' + body.message)
    callback(body)
})
},

getProduct: (token, base_url, sku, callback) => {
    var url = base_url + '/V1/products/' + sku
    var n = 0
    var body
    async.doUntil(
        (check) => {
        if(n++ > 0) console.log(`retrying prod search(${n})`)
    request({
        method: 'GET',
        url: url,
        auth: {'bearer': token},
        json: true
    }, (e, r, b) => {
        if(r.statusCode == 400)
    check(b.message)
else check(null, e, r, b)
})
}, (err, resp, body) => {
        if(err) { console.error(err); return false; }
        if(resp.statusCode != 200) { console.error(resp.statusCode); return false; }
        if(body.message) { console.error(body.message); return false; }
        callback(body)
        return true
    }, (err) => {
        if(err) console.error(err)
    }
)
},

getOrderItem: (token, base_url, data, callback) => {
    request({
        method: 'GET',
        url: base_url + '/V1/orders/items/' + data,
        auth: {'bearer': token},
        json: true
    }, (e, r, body) => {
        if (r.statusCode != 200)
            console.error(r.statusCode)
        if(e)
            console.error('getOrderItem error:', e)
        if(body.message)
            console.error(body.message)
        callback(body)
    })
},

updateOrder: (token, base_url, order_id, data, logfile, callback) => {
    /**
     * update some custom fields
     */
    var postbody = {
        "custom_data": data
    }
    request({
        method: 'PUT',
        url: base_url + '/V1/order/' + order_id + '/update',
        auth: { 'bearer': token },
        body: postbody,
        json: true,
        gzip: true
    }, (e, r, body) => {
        //console.log(/*'p:', postbody, */ 'e:', e, 'r:', r.statusCode, 'b:', body)
        var msg
        if (e || r.statusCode != 200) {
        msg = 'update order error: '+JSON.stringify(body)+"\n"+'tried: '+JSON.stringify(postbody)
    } else {
        msg = 'done!'
    }
    callback(body)
})
},

updateQuote: (token, base_url, quote_id, data, logfile, callback) => {
    /**
     * update some custom fields
     */
    var postbody = {
        "custom_data": data
    }
    request({
        method: 'PUT',
        url: base_url + '/V1/quote/' + quote_id + '/update',
        auth: { 'bearer': token },
        body: postbody,
        json: true,
        gzip: true
    }, (e, r, body) => {
        var msg
        if (e || r.statusCode != 200) {
        msg = 'update quote error: '+JSON.stringify(body)+"\n"+'tried: '+JSON.stringify(postbody)
    } else {
        msg = 'done!'
    }
    callback(body)
})
},

/*updateCustomer: (customer, callback) => {
 /!**
 * restore cust email
 *!/
 process.stdout.write("\nrestore cust email...");
 var postbody = {
 'customer': {
 'id': customer.id,
 'email': customer.email,
 'firstname': customer.firstname,
 'lastname': customer.lastname,
 'websiteId': 1
 }
 }
 request({
 method: 'PUT',
 url: self.config.magento_base_url + '/V1/customers/'+customer.id,
 auth: { 'bearer': token },
 headers: { 'content-type': 'application/json' },
 body: postbody,
 json: true
 }, (err, response, body) => {
 if(err || response.statusCode!=200) {
 console.error('restore email error:', body)
 console.error('tried:', postbody)
 } else {
 process.stdout.write("done!\n");
 callback(body)
 }
 })
 },*/

updateProduct: (token, base_url, sku, data, logfile, callback) => {
    request({
        method: 'PUT',
        url: base_url + '/V1/products/' + sku,
        auth: { 'bearer': token },
        headers: { 'content-type': 'application/json' },
        body: data,
        json: true,
        gzip: true
    }, (err, response, body) => {
        if(err || response.statusCode!=200) {
        console.error('update prod error: '+body.message)
        if(err) console.error(err)
        callback(false)
    } else {
        if(err) console.error(err)
        callback(true)
    }
})
},

orderComment: (token, base_url, order_id, data, logfile, callback) => {
    request({
        method: 'POST',
        url: base_url + '/V1/orders/' + order_id + '/comments',
        auth: { 'bearer': token },
        headers: { 'content-type': 'application/json' },
        body: data,
        json: true,
        gzip: true
    }, (e, r) => {
        if(e || r.statusCode!=200) {
        if(err) console.error(err)
        callback(false)
    } else {
        callback(true)
    }
})
},

createOrder: (token, base_url, store_id, order, processed, cur_cust, cur_prod, logfile, callback) => {
    var customer = null
    var cart_id = null
    var order_id = null
    var order_items = [] // magento order item IDs
    var missed_items = []
    var missed_promo = false
    var bad_email = false
    var credit = false
    async.waterfall([
            (cb) => {
            /**
             * search prods
             */
            var skus = []
            for(var item of order.items) {
        skus.push(item.sku)
    }
    var qs = {
        'searchCriteria[filterGroups][0][filters][0][field]': 'sku',
        'searchCriteria[filterGroups][0][filters][0][value]': skus.join(),
        'searchCriteria[filterGroups][0][filters][0][conditionType]': 'in'
    }
    var url = base_url + '/V1/products'
    request({
        method: 'GET',
        url: url,
        auth: { 'bearer': token },
        qs: qs,
        json: true
    }, (err, response, body) => {
        if(err || response.statusCode!=200) {
        if(err) console.error(err, `tried ${url}`)
        var sc = response.statusCode || null;
        var msg = 'createOrder error ('+sc+') on prod search / '
        msg += 'qs: '+JSON.stringify(qs)
        var bdy = JSON.stringify(body)
        if(body) msg += ' / ' + bdy
        callback({message: msg})
    } else {
        var found_items = []
        for (var item of order.items) {
            var missed = true
            for(var found of body.items) {
                if(found.sku==item.sku) {
                    missed = false
                    break
                }
            }

            if(missed) {
                missed_items.push(item)
            } else {
                found_items.push(item)
            }
        }

        order.items = found_items
        cb()
    }
})
}, (cb) => {
        /**
         * search for customer
         */
        var url = base_url + '/V1/customers/search'
        request({
            method: 'GET',
            url: url,
            auth: {'bearer': token},
            qs: {
                'searchCriteria[filterGroups][0][filters][0][field]': 'email',
                'searchCriteria[filterGroups][0][filters][0][value]': order.email,
                'searchCriteria[filterGroups][0][filters][0][conditionType]': 'eq',
                'searchCriteria[filterGroups][1][filters][1][field]': 'store_id',
                'searchCriteria[filterGroups][1][filters][1][value]': store_id,
                'searchCriteria[filterGroups][1][filters][1][conditionType]': 'eq'
            },
            json: true
        }, (err, response, body) => {
            if(err || response.statusCode!=200) {
            console.error('search cust error: '+JSON.stringify(body)+"\n"+'tried: '+url)
            cb()
        } else  {
            if(body.items.length>0) {
                customer = body.items[0]
            }
            cb()
        }
    })
    }, (cb) => {
        /**
         * create customer if needed
         */
        var cid = customer ? customer.id : 0
        if(cid > 0) {
            cb()
        } else {
            var address = Object.assign({}, order.addressInformation.billingAddress)
            if (order.dealer)
                address.company = order.dealer
            // find region Id
            for (var region of self.config.magento_regions_us) {
                if (region.code == address.regionCode) {
                    address.regionId = region.id
                    break
                }
            }
            delete address.regionCode
            //address.defaultShipping = true
            //address.defaultBilling = true
            var postbody = {
                'customer': {
                    'email': order.email,
                    'firstname': order.firstname,
                    'lastname': order.lastname,
                    'group_id': order.dealer_id ? 2 : 1,
                    'website_id': order.mage_site_id,
                    'addresses': [address],
                    "extension_attributes": {
                        "is_subscribed": order.newsletter
                    }
                }
            }
            var url = base_url + '/V1/customers/' + cid
            request({
                method: 'PUT',
                url: url,
                auth: {'bearer': token},
                headers: {'content-type': 'application/json'},
                body: postbody,
                json: true,
                gzip: true
            }, (err, response, body) => {
                if (err || response.statusCode != 200) {
                console.log(`create cust error`.blue, order.email)
                if (err) console.error(err)
                console.log(body)

                // bad email? just use placeholder customer for now, leave note?
                if (body && typeof body.message != 'undefined') {
                    if (body.message.includes('valid')) {
                        bad_email = order.email
                        customer = {'id': 15}
                        console.log("!!! using info@ !!!")
                        cb()
                    } else {
                        callback(body)
                    }
                } else {
                    callback(body)
                }
            } else {
                customer = body
                cb()
            }
        })
        }
    }, (cb) => {
        /**
         * thread blocking efforts
         */
        // make sure order hasn't been processed in the mean time
        /*if(processed.indexOf(order.id) > -1) {
         console.info(' -- Order already processed');
         callback({ message: 'Order already processed' })
         } else {*/
        //cur_cust.push(customer.id)
        //processed.push(order.id)
        cb()
        //}
    }, (cb) => {
        /**
         * make cart
         */
        var url = base_url + '/V1/customers/' + customer.id + '/carts'
        request({
            method: 'POST',
            url: url,
            auth: {'bearer': token},
            json: true,
            gzip: true
        }, (err, response, body) => {
            if(err || response.statusCode!=200) {
            console.error('make cart error: '+JSON.stringify(body)+"\n"+'tried: '+url)

            // free cust
            var i = cur_cust.indexOf(customer.id)
            cur_cust.splice(i, 1)

            callback(body)
        } else {
            cart_id = body
            cb()
        }
    })
    }, (cb) => {
        /**
         * add items to cart
         */
        if(order.items.length>0) {
            var postbody = {'products': []}
            async.eachSeries(order.items, (item, cb2) => {
                /**
                 * activate if inactive
                 */
                if (item.active == 'n') {
                missed_items.push(item)
                cb2()
            } else {
                postbody.products.push({
                    sku: item.sku,
                    qty: item.qty,
                    price: item.price
                })
                cb2()
            }
        }, (err) => {
                if (err) {
                    callback(err)
                } else {
                    /**
                     * add em
                     */
                    request({
                        method: 'POST',
                        url: base_url + '/V1/carts/' + cart_id + '/items-multi',
                        auth: {'bearer': token},
                        headers: {'content-type': 'application/json'},
                        body: postbody,
                        json: true,
                        gzip: true
                    }, (err, response, body) => {
                        if (err || response.statusCode != 200) {
                        console.error(`${response.statusCode} add items error: ${body.message} ${JSON.stringify(postbody)}`)

                        // restore item status'
                        async.eachSeries(order.items, (item, cb3) => {
                            missed_items.push(item)
                        cb3()
                    }, cb)
                    } else {
                        cb()
                    }
                })
                }
            })
        } else {
            // nothing to add
            console.info('only dummy items...skip these for now'.blue)
            callback({message:'only dummy items'})
        }
    }, (cb) => {
        /**
         * spoof missing items
         */
        async.eachSeries(missed_items, (item, nextItem) => {
            console.log('spoof item '+item.sku+' (cart '+cart_id+')')
        // dummy prod will store real sku in custom option for reference
        var postbody = {
            "cartItem": {
                "sku": "dummyproduct",
                "qty": item.qty,
                "price": item.price,
                "quoteId": cart_id,
                "productOption": {
                    "extensionAttributes": {
                        "customOptions": [
                            {
                                "optionId": "1", // dummy prod custom field, titled SKU
                                "optionValue": item.sku
                            }
                        ]
                    }
                }
            }
        }
        request({
            method: 'POST',
            url: base_url + '/V1/carts/' + cart_id + '/items',
            auth: {'bearer': token},
            headers: {'content-type': 'application/json'},
            body: postbody,
            json: true,
            gzip: true
        }, (err) => {
            nextItem(err)
        })
    }, (err) => {
            if(err) console.error(err)
            else cb()
        })
    }, (cb) => {
        /**
         * apply promo code if needed
         */
        if(order.promo) {
            missed_promo = true
            cb()
        } else {
            if(order.discount>0)
                credit = true
            cb()
        }
    }, (cb) => {
        /**
         * set shipping
         */
        var addressInformation = JSON.parse(JSON.stringify(order.addressInformation))
        if(order.pickup) {
            addressInformation.shippingAddress = {
                "regionCode": 'FL',
                "regionId": 18,
                "countryId": "US",
                "street": [ 'n/a' ],
                "telephone": 'n/a',
                "postcode": 'n/a',
                "city": 'n/a',
                "firstname": 'n/a',
                "lastname": 'n/a'
            }
        }
        // fix for mageside newsletter extension
        addressInformation.extension_attributes = {
            "newsletter_subscribe": false
        }
        var postbody = { 'addressInformation': addressInformation }
        var url = base_url + '/V1/carts/' + cart_id + '/shipping-information'
        request({
            method: 'POST',
            url: url,
            auth: {'bearer': token},
            headers: {'content-type': 'application/json'},
            body: postbody,
            json: true,
            gzip: true
        }, (err, response, body) => {
            if (err || response.statusCode != 200) {
            console.error('ship error ('+response.statusCode+'): '+body+"\n"+'tried: '+JSON.stringify(postbody))
            console.error('url: '+url)
            // free cust
            var i = cur_cust.indexOf(customer.id)
            cur_cust.splice(i, 1)

            callback(body)
        } else {
            cb()
        }
    })
    }, (cb) => {
        /**
         * replace email with fake to circumvent order confirmation
         */
        var spoof_email = 'magentosyncbs@victorytailgate.com'
        var postbody = {custom_data: {'customer_email': spoof_email }}
        request({
            method: 'PUT',
            url: base_url + '/V1/quote/' + cart_id + '/update',
            auth: { 'bearer': token },
            headers: { 'content-type': 'application/json' },
            body: postbody,
            json: true,
            gzip: true
        }, (err, response, body) => {
            if(err || response.statusCode!=200) {
            console.log('quote update error'.blue)
            if(err) console.error(err)
            else console.error(response.statusCode+') quote update error: '+body.message+"\n"+'tried:'+JSON.stringify(postbody))
            callback(body)
        } else {
            cb()
        }
    })
    }, (cb) => {
        /**
         * create order
         */
        // check payment methods...
        /*var options = {
         method: 'GET',
         url: base_url+'/V1/carts/'+cart_id+'/payment-methods',
         headers: {
         'cache-control': 'no-cache',
         authorization: 'Bearer '+token,
         'content-type': 'application/json'
         }, json: true
         };
         request(options, function () {*/
        var match = false
        // sometimes free doesn't work even though grand total is 0...
        if(/*!match &&*/ order.paymentMethod.method == 'free') {
            var ponum = order.paymentMethod.poNumber != 'undefined' ? order.paymentMethod.poNumber : 'none'
            //console.log('order method free, using purchaseorder...')
            order.paymentMethod = { poNumber: ponum, method: 'purchaseorder' }
        }
        console.log(order.paymentMethod)

        var url = base_url + '/V1/carts/' + cart_id + '/order'
        var postbody = { 'paymentMethod': order.paymentMethod }
        request({
            method: 'PUT',
            url: url,
            auth: {'bearer': token},
            body: postbody,
            json: true,
            gzip: true
        }, (err, response, body) => {
            if (err || response.statusCode != 200) {
            console.error('create order error, tried: ' + JSON.stringify(postbody))
            console.error('url: '+url)
            console.info('cart id: '+cart_id)

            // free cust
            var i = cur_cust.indexOf(customer.id)
            cur_cust.splice(i, 1)

            callback(body)
        } else {
            order_id = body
            console.log('order created:', order_id)

            /**
             * update order with correct financials
             */
            var custom_data = {
                "vt_order_id": order.id,
                "created_at": order.created,
                "marketplace_id": order.site,
                "dealer_id": order.dealer_id,
                "base_grand_total": order.total,
                "grand_total": order.total,
                "base_subtotal": order.subtotal,
                "subtotal": order.subtotal,
                "base_subtotal_incl_tax": order.subtotal + order.tax,
                "subtotal_incl_tax": order.subtotal + order.tax,
                "base_shipping_amount": order.shipping,
                "shipping_amount": order.shipping,
                "base_shipping_incl_tax": order.shipping + order.tax,
                "shipping_incl_tax": order.shipping + order.tax,
                "base_tax_amount": order.tax,
                "tax_amount": order.tax,
                "base_discount_amount": order.discount,
                "discount_amount": order.discount,
                "base_total_due": order.status == 2 || order.status == 7 ? 0 : order.total,
                "total_due": order.status == 2 || order.status == 7 ? 0 : order.total
            }
            if (order.comment)
                custom_data.customer_note = order.comment
            console.log('update order...')
            self.updateOrder(token, base_url, order_id, custom_data, logfile, (result) => {
                if (order.status == 4) {
                // mark canceled in magento and exit
                // free cust
                var i = cur_cust.indexOf(customer.id)
                cur_cust.splice(i, 1)

                // cancel order
                var url = base_url + '/V1/orders/' + order_id + '/cancel'
                request({
                    method: 'POST',
                    url: url,
                    auth: {'bearer': token},
                    headers: {'content-type': 'application/json'},
                    json: true,
                    gzip: true
                }, (e, r) => {
                    if (e || (r.statusCode != 200 && r.statusCode != 504)/* || body !== true*/) {
                    var msg = '('+r.statusCode+') order cancel error: ' + "\n" + 'tried: ' + url
                    callback({'message': msg})
                } else {
                    if(r.statusCode==504) {
                        // timed out bout probably worked
                        console.error('504, check ' + order_id)
                    }

                    // free cust
                    var i = cur_cust.indexOf(customer.id)
                    cur_cust.splice(i, 1)

                    // fix order email back
                    console.log('fix email...')
                    var data = {'customer_email': customer.email}
                    self.updateOrder(token, base_url, order_id, data, logfile, () => {
                        callback(parseInt(order_id))
                })
                }
            })
            } else {
                cb()
            }
        })

            // update quote with vto
            custom_data = { "vt_order_id": order.id }
            self.updateQuote(token, base_url, cart_id, custom_data, logfile, ()=>{})
        }
    });
        //})
    }, (cb) => {
        /**
         * commentate missing items / failed promo code
         */
        if(missed_items.length>0 || missed_promo) {
            var comment = ''
            if(bad_email)
                comment += 'Invalid original email: '+bad_email+'<br>'
            var missing = []
            for(var item of missed_items)
                missing.push(`${item.qty} ${item.name} - ${item.sku}`)
            comment += `Missing items: <br>${missing.join("<br>")}`
            if(missed_promo)
                comment += `<br>Missed promo code: ${order.promo.coupon.code}`
            var data = {
                "statusHistory": {
                    "comment": comment,
                    "isCustomerNotified": 0,
                    "isVisibleOnFront": 0,
                    "extensionAttributes": {}
                }
            }
            console.log('make order comment...')
            self.orderComment(token, base_url, order_id, data, logfile, () => {
                cb()
            })
        } else {
            cb()
        }
    }, (cb) => {
        console.log('invoice order...')
        if(order.status==2 || order.status==7) {
            /**
             * invoice order
             */
            var postbody = {
                "capture": false,
                "notify": false,
                "appendComment": true,
                "comment": {
                    "comment": "original payment type: " + order.ptype + " / original transaction id: " + order.trans_id
                }
            }
            request({
                method: 'POST',
                url: base_url + '/V1/order/' + order_id + '/invoice',
                auth: {'bearer': token},
                body: postbody,
                json: true,
                gzip: true
            }, (err, response, body) => {
                if (err || response.statusCode != 200) {
                console.error('invoice error: ' + JSON.stringify(body) + "\n" + 'tried: ' + JSON.stringify(postbody))
            }
            cb()
        })
        } else {
            cb()
        }
    }, (cb) => {
        /**
         * get order items
         */
        request({
            url: base_url + '/V1/orders/' + order_id,
            auth: {'bearer': token},
            json: true
        }, (e, r, body) => {
            if (e || r.statusCode != 200) {
            console.error('get order error: '+JSON.stringify(body))
        } else {
            order_items = body.items
            cb()
        }
    })
    }, (cb) => {
        /**
         * ship order
         */
        console.log('ship order...')
        if(order.status==3) {
            // just ship em all under the one
            var postbody = {
                "notify": false,
                "appendComment": true,
                "items": [],
                "tracks": []
            }
            for (var item of order_items) {
                postbody.items.push({ orderItemId: item.item_id, qty: item.qty_ordered })
            }
            if(order.order_tracking) {
                for (var tracking_num of order.order_tracking) {
                    var carrier = order.order_carrier || 'fedex'
                    postbody.tracks.push({ 'trackNumber': tracking_num, 'carrierCode': carrier })
                }
            } else {
                // just make one up got dammit
                postbody.tracks.push({ 'trackNumber': '0123456789', 'carrierCode': 'fedex' })
            }
            var url = base_url + '/V1/order/' + order_id + '/ship'
            request({
                method: 'POST',
                url: url,
                auth: { 'bearer': token },
                headers: { 'content-type': 'application/json' },
                body: postbody,
                json: true,
                gzip: true
            }, (err, response, body) => {
                if (err || response.statusCode != 200) {
                console.error('ship error: '+JSON.stringify(body)+"\n"+'tried: '+JSON.stringify(postbody))
                console.error('url: '+url)
            } else {
                cb()
            }
        })
        } else {
            // nothing to ship
            cb()
        }
    }
], (err) => {
        if(err)
            console.error('order sync error: ', err)

        // free cust
        var i = cur_cust.indexOf(customer.id)
        cur_cust.splice(i, 1)

        // fix order email back
        console.log('restore email...')
        var data = { 'customer_email': customer.email }
        self.updateOrder(token, base_url, order_id, data, logfile, () => {
            callback(parseInt(order_id))
    })
    })
}
}

module.exports = function(config) {
    self.config = config
    return self;
}