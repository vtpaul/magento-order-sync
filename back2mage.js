// ** Requires **
process.stdout.isTTY = true // This makes colors work in webstorm "oowee ..?"
process.env.TZ = 'America/New_York Time Zone'

const async     = require('async')
const colors    = require('colors').enabled = true
const request   = require('request')
const mysql     = require('mysql')
const express   = require('express')
const jsonfile  = require('jsonfile')
const fs        = require('fs')
const url       = require('url')
const moment    = require('moment-timezone')

// ** Load The Configs **
const config = require('./config.json');
const pool  = mysql.createPool(require('./' + config.mysql_config))

// ** Load Magento 'class' **
const magento = require('./magento.js')(config)

const maps = {
    payment_types: {
        authorizenet_directpost: 1,
        authnetcim: 1,
        paypal_express: 2,
    }
}

const LOOKBACK = 300

const syncOrdersLegacy = (token, base_url, store_id, site_ids, search_id, site, nextFunc) => {
    sendHeartbeat()

    /**
     * VT to Magento
     */
    var orders = {}
    var order_container = []
    var search = search_id ?
        `o.order_id = ${search_id}` :
        "o.date_added >= '2017-10-01 00:00:00' and o.site IN (?) and o.status NOT IN (0,1)"

    var sql_get_orders = `
        select
          o.*,
          i.item_id,
          i.product_id,
          i.product_type,
          i.quantity,
          i.price,
          i.name as item_name,
          group_concat(distinct ot.tracking_num order by ot.tracking_num) as order_tracking,
          group_concat(lower(ot.carrier) order by ot.tracking_num) as order_carrier,
          concat(ot2.tracking_num,':',lower(ot2.carrier),':',it.quantity_shipped) as item_tracking,
          if(max(p.amount_off)>0, max(p.amount_off), if(max(p.percent_off)>0, max(p.percent_off), if(o.discount>0, o.discount, null))) as 'promo_value',
          if(max(p.amount_off)>0, 'cart_fixed', if(max(p.percent_off)>0, 'by_percent', if(o.discount>0, 'cart_fixed', null))) as 'promo_type',
          max(c.newsletter) as newsletter, 
          x.mage_order_id
        from ordered_items i
          inner join orders o on o.order_id=i.order_id
          inner join products d on d.id=i.product_id
          left join vtorder_mageorder x on x.vt_order_id=o.order_id
          left join order_tracking ot on ot.order_id=o.order_id
          left join ordered_item_tracking it on it.ordered_item_id=i.item_id
          left join order_tracking ot2 on ot2.track_id=it.track_id
          left join promos p on p.code=o.promo_id
          left join customers c on c.email=o.email
        where
            (x.mage_order_id is null) 
            and ${search}
            and i.order_id not in (785997,785936,785936,785922,785904,785900,785637,784451,780515)
        group by i.item_id
        order by o.order_id asc
        limit 100;
    `;

    var connection = null
    async.waterfall([
        (cb) => {
            pool.getConnection(cb)
        }, (new_connection, cb) => {
            connection = new_connection
            console.log("Legacy to Magento".yellow)
            console.log(moment().format())
            var q = connection.query(sql_get_orders, [site_ids], cb)
        }, (rows) => {
            for(var row of rows) {
                // initialize order obj if necessary
                if(typeof(orders[row['order_id']])=='undefined') {
                    var pickup = false
                    if(row['shipping_method'].toLowerCase()=='pick up' || row['shipping_address1'].toLowerCase()=='pick up' || row['shipping_city'].toLowerCase()=='pick up')
                        pickup = true
                    var payment_method = parseFloat(row['total'])>0 ? 'purchaseorder' : 'free'
                    var payment_type = 'unknown'
                    if(row['payment_type']==1)
                        payment_type = 'authorizenet_directpost'
                    else if(row['payment_type']==2)
                        payment_type = 'paypal_express'
                    var trans_id = row['transaction_id'] || 'n/a'
                    var po_number = row['site_order_num'] || row['customer_po']
                    if(!row['shipping_last_name'] || /^\s*$/.test(row['shipping_last_name'])) {
                        row['shipping_last_name'] = 'n/a'
                    }

                    var promo = false
                    if(row['promo_id']!='' && row['promo_value']!=null) {
                        promo = {
                            'rule': {
                                "name": row['promo_id'],
                                "description": row['promo_id'],
                                "websiteIds": [ 1 ], // vt
                                "customerGroupIds": [ 1 ], // general
                                "usesPerCustomer": 0,
                                "isActive": true,
                                "stopRulesProcessing": false,
                                "isAdvanced": true,
                                "simpleAction": row['promo_type'],
                                "discountAmount": row['promo_value'],
                                "applyToShipping": false,
                                "isRss": true,
                                "couponType": "SPECIFIC_COUPON",
                                "useAutoGeneration": false
                            },
                            'coupon': {
                                "code": row['promo_id'],
                                "is_primary": true
                            }
                        }
                    }
                    var shipping = {
                        "regionCode": pickup ? 'FL' : (row['shipping_state'].toUpperCase() || 'FL'),
                        "countryId": "US",
                        "street": [ row['shipping_address1'].trim() || 'n/a', row['shipping_address2'].trim() || '' ],
                        "telephone": row['phone'] || 'n/a',
                        "postcode": row['shipping_zip'] || 'n/a',
                        "city": pickup ? 'FL' : (row['shipping_city'] || 'n/a'),
                        "firstname": row['shipping_first_name'] || 'n/a',
                        "lastname": row['shipping_last_name'] || 'n/a'
                    }
                    var billing = row['billing_first_name']=='' ? shipping : {
                        "regionCode": row['billing_state'].toUpperCase() || 'FL',
                        "countryId": "US",
                        "street": [ row['billing_address1'].trim() || 'n/a', row['billing_address2'].trim() || '' ],
                        "telephone": row['phone'] || 'n/a',
                        "postcode": row['billing_zip'],
                        "city": row['billing_city'],
                        "firstname": row['billing_first_name'],
                        "lastname": row['billing_last_name'] || 'n/a'
                    }
                    // mysql server seems to be in UTC but we store EST string dates in it
                    // the result is a EST time that js thinks is in UTC, and thus 4 hours behind
                    // workaround by adding 4 hours to make it actually utc
                    var created_est = moment(row['date_added']).add(4, 'hours') // real utc
                    orders[row['order_id']] = {
                        'id': row['order_id'],
                        'created': created_est,
                        'firstname': row['name'].trim().split(' ')[0] || row['billing_first_name'] || row['shipping_first_name'] || 'n/a',
                        'lastname': row['name'].trim().split(' ').splice(-1)[0] || row['billing_last_name'] || row['shipping_last_name'] || 'n/a',
                        'email': row['email'] || 'info@victorytailgate.com',
                        'dealer': row['dealer_id'] > 0 ? row['business_name'] : false,
                        'dealer_id': row['dealer_id'] > 0 ? row['dealer_id'] : null,
                        'site': row['site'] > 0 ? row['site'] : null,
                        'mage_site_id': config[`store_id_${site}`],
                        'newsletter': row['newsletter'] > 0,
                        "items": [],
                        "addressInformation": {
                            "shippingAddress": shipping,
                            "billingAddress": billing,
                            "shippingMethodCode": /*pickup ? 'freeshipping' :*/ "flatrate", //shipment!=null ? shipment.selected_rate.service : "flatrate",
                            "shippingCarrierCode": /*pickup ? 'freeshipping' :*/ "flatrate" //carrier
                        },
                        "paymentMethod": {
                            "poNumber": po_number || 'none',
                            "method": payment_method
                        },
                        'ptype': payment_type,
                        'trans_id': trans_id,
                        'comment': row['comments'] || null,
                        'pickup': pickup,
                        'discount': row['discount'] > row['subtotal']+row['shipping']+row['tax'] ? row['subtotal']+row['shipping']+row['tax'] : row['discount'],
                        'promo': promo,
                        'status': row['status'],
                        'subtotal': parseFloat(row['subtotal']),
                        'total': parseFloat(row['total']),
                        'shipping': parseFloat(row['shipping']),
                        'tax': parseFloat(row['tax']),
                        'item_tracking': row['item_tracking']!=null,
                        'order_tracking': row['order_tracking'] ? row['order_tracking'].split(',') : null,
                        'order_carrier': row['order_carrier'] ? row['order_carrier'].split(',')[0] : null,
                        'mid': row['mage_order_id']
                    }

                    order_container.push(orders[row['order_id']])
                }

                var board = config.board_types.indexOf(row['product_type']) > -1

                // push new item
                orders[row['order_id']].items.push({
                    "name": row['item_name'],
                    "sku": row['product_id'].toString(),
                    "qty": board ? row['quantity']*2 : row['quantity'],
                    "price": board ? (row['price']/2) : row['price'],
                    'tracking': row['item_tracking'] ? row['item_tracking'].split(',') : [],
                    'active': row['active'],
                    'item_id': row['item_id']
                })
            }

            var count = Object.keys(orders).length;
            console.log(count + ' orders to sync')

            var i = 1
            var complete = 0
            var limit = 1

            // dem logs doh
            var logfilemaster = __dirname + '/ordersync_legacy.log'
            var logfiles = []

            var to
            async.eachSeries(order_container, (order, cb2) => {
                var logfile = logfilemaster

                i++
                console.log("\x1b[33m" + 'processing order ' + order.id + "\x1b[0m")

                var eid = null

                var q = connection.query("insert into vtorder_mageorder (vt_order_id, mage_order_id, mage_site, legacy) values (?, ?, ?, ?)", [order.id, 0, site, 1], (err, result) => {
                    //console.log(q.sql)
                    if(err) console.error(err)
                    /*else console.log('placeholder link inserted')*/

                    magento.createOrder(token, base_url, store_id, order, processed, cur_cust, cur_prod, logfile, (result) => {
                        //console.log(`createOrder result: ${JSON.stringify(result)}`)
                        async.waterfall([
                            (cb3) => {
                                //console.log('pause...')
                                setTimeout(cb3, 1000)
                            }, (cb3) => {
                                if (typeof(result) !== "number") {
                                    if (typeof(result) != 'undefined' && typeof(result.eid) != 'undefined')
                                        eid = result.eid

                                    var msg
                                    if (typeof(result) != 'undefined' && typeof(result.message) != 'undefined') {
                                        msg = order.id + ': ' + result.message
                                    } else {
                                        msg = 'unknown error on ' + order.id
                                    }
                                    console.error('msg: '+msg)

                                    connection.query(`update vtorder_mageorder set note = ? where mage_order_id = 0 and vt_order_id = ? limit 1`, [msg, order.id], (err, result)=>{
                                        if(err) console.error(err)
                                        else console.log('error noted'.blue)
                                        cb3()
                                    })
                                } else {
                                    eid = result
                                    //console.log('mage entity id: '+eid)
                                    cb3()
                                }
                            }, (cb3) => {
                                // update VT record
                                if (eid) {
                                    // get full order info from magento
                                    //console.log('get full mage order...')
                                    magento.getOrder(token, base_url, eid, (mage_order) => {
                                        var q
                                        var comment = "Magento: <a target='_blank' href='https://www.victorytailgate.com/admin_zzvddg/sales/order/view/order_id/" + eid + "'>#" + mage_order.increment_id + "</a><br><br>"
                                        console.log('add comment to backoffice')
                                        q = connection.query("update orders set comments = concat(?, comments) where order_id = ? limit 1", [comment, order.id], (err) => {
                                            if(err) console.error(err)

                                            //console.log('updating order link...')
                                            q = connection.query("update vtorder_mageorder set mage_order_id = ?, mage_incr_id = ? where vt_order_id = ? and mage_site = ? and mage_order_id = 0 limit 1", [eid, mage_order.increment_id, order.id, site], (err) => {
                                                if (err) {
                                                    console.error('msyql error:', err)
                                                    cb3()
                                                } else {
                                                    //console.log('link items...')
                                                    complete++

                                                    // link ordered items
                                                    var item_values = []

                                                    /*for (var item of order.items) {
                                                     for (var k = 0; k < item.qty; k++)
                                                     item_values.push(`(${item.sku}, '${site}', ${order.id}, ${item.item_id}, ${eid})`)
                                                     }*/
                                                    console.log(order.items.length + ' items')
                                                    async.eachSeries(order.items, (item, nextItem) => {
                                                        console.log(JSON.stringify(item))
                                                        var k = 0
                                                        async.whilst(
                                                            () => { return k < item.qty },
                                                            (next) => {
                                                                item_values.push(`(${item.sku}, '${site}', ${order.id}, ${item.item_id}, ${eid})`)
                                                                //console.log('item pushed')
                                                                k++
                                                                next(null, k)
                                                            }, () => {
                                                                nextItem()
                                                            }
                                                        )
                                                    }, (err) => {
                                                        //console.log('link query...')
                                                        if (item_values.length > 0) {
                                                            var values = item_values.join(',')
                                                            connection.query(`insert into vtorderitem_mageorderitem (sku, mage_site, vt_orderid, vt_itemid, mage_orderid) values ${values}`, [], (err, result) => {
                                                                if (err) console.error(err)

                                                                var mp = order.site == null ? 0 : order.site
                                                                var msg = "\x1b[32m" + (i - 1) + ') order ' + order.id + "[" + mp + "]/" + eid + " OK - " + complete + "/" + count + "\x1b[0m"
                                                                console.log(msg)

                                                                cb3()
                                                            })
                                                        } else {
                                                            console.log('none to link?')
                                                            cb3()
                                                        }
                                                    })
                                                }
                                            })
                                        })
                                    })
                                } else {
                                    cb3()
                                }
                            }
                        ], (err) => {
                            if (err) console.error(err)
                            else console.log('next order...')
                            setTimeout(cb2, 1000)
                        })
                    })
                })
            }, (err) => {
                if(err) console.error('eachLimit error:', err)
                console.log("--- done ---".yellow)

                if(nextFunc)
                    setTimeout(()=>{nextFunc(token, base_url, config[`store_id_${site}`], site_ids, false, site, syncOrdersLegacy)}, 30000)

                connection.release()
            })
        }
    ], (err) => {
        if(err) {
            console.error(err)
            connection.release()
        }
    })
}

const getMetadata = (file, callback) => {
    var metadata = {}
    jsonfile.readFile(file, (err, obj) => {
        if (err && err.errno!=-2) {
            console.error(err)
        } else {
            metadata = obj
            callback(null, metadata)
        }
    })
}

const sendHeartbeat = () => {
    var name = site=='k12' ? 'OrderSyncK12' : 'OrderSync'
    request({
        method: 'GET',
        url: 'https://api.opsgenie.com/v2/heartbeats/'+name+'/ping',
        headers: { 'content-type': 'application/json', 'authorization': 'GenieKey c8715792-5166-480b-8983-a21976478dda' },
        json: true
    })
}

const sendAlert = (msg) => {
    var body = {
        "message": 'Mage order sync error',
        "description": msg,
        "teams": [{"name": "Web"}],
        "tags": ["Magento"],
        "priority": "P3"
    }
    request({
        method: 'POST',
        url: 'https://api.opsgenie.com/v2/alerts',
        headers: { 'content-type': 'application/json', 'authorization': 'GenieKey ffcb3c34-e6d0-47c3-b1a6-a78659e530bf' },
        body: JSON.stringify(body)
    }, (err, r) => {
        if(err) console.error(err)
    })
}

const insertVtItem = (connection, connection_mage, token, item, callback) => {
    console.log(`insert ${item.qty_ordered} item(s)...`)
    if(!item.sku) {
        console.error('bad sku, skipping'.red)
        callback()
        return
    }

    let custom = 'custom' in item

    var custom_info = false
    var itemids = []
    var mageitem

    async.waterfall([
        (insertStep) => {
            connection.query('select custom_text from products where id = ?', [item.sku], (err, result) => {
                insertStep(null, result[0])
            })
        }, (vtprod, insertStep) => {
            /**
             * get magento item
             */
            var mage_item_id = 'parent_item' in item ? item.parent_item.item_id : item.item_id
            magento.getOrderItem(token, base_url, mage_item_id, (orderitem) => {
                mageitem = orderitem
                if(item.qty_ordered) mageitem.qty_ordered = item.qty_ordered
                mageitem.item_status = mageitem.qty_refunded == mageitem.qty_ordered ? 11 // full refund
                    : mageitem.qty_refunded > 0 ? 10 // partial refund
                    : mageitem.qty_canceled > 0 ? 8 // canceled
                    : mageitem.qty_shipped == mageitem.qty_ordered ? 7 // shipped
                    : 0 // unknown

                if(/*(vtprod && vtprod.custom_text == 1) ||*/ item.name.match(/custom/i))
                    mageitem.item_status = 19 // pending customization

                if(typeof orderitem.product_option != 'undefined') {
                    custom_info = ''
                    var options

                    /**
                     * check for giftcard
                     */
                    options = orderitem.product_option.extension_attributes.giftcard_item_option
                    if(typeof options != 'undefined') {
                        /** LOOKS LIKE:
                         "giftcard_item_option": {
                            "giftcard_amount": "200",
                            "giftcard_sender_name": "Karen Northcutt",
                            "giftcard_recipient_name": "Sarah Coll",
                            "giftcard_sender_email": "northcutt.karen@gmail.com",
                            "giftcard_recipient_email": "sarah@mmc3.com"
                        }*/
                        custom_info += JSON.stringify(options)
                    }

                    /**
                     * check custom text
                     */
                    options = orderitem.product_option.extension_attributes.custom_options
                    if(typeof options != 'undefined') {
                        console.log(`customization found`.magenta)

                        mageitem.item_status = 19 // pending customization

                        // set order to pending if needed
                        //connection.query("update orders set status = 1 where order_id = ? and status != 1 limit 1;", [item.order_id])

                        custom_info += `</span><br><br><span>Customization:`
                        //for (var option of options) {
                        async.eachOfSeries(options, (option, okey, nextOption) => {
                            //console.log(`${okey} option: ${JSON.stringify(option)}`)
                            if (option.option_value.indexOf('image') > -1) {
                                var vals = option.option_value.split(',')
                                /**
                                 * LOOKS LIKE:
                                 * [
                                 * image/png,
                                 * Nascar.png,
                                 * custom_options/quote/N/a/a38967c57daaafde298907b3670fb83f.png,
                                 * custom_options/order/N/a/a38967c57daaafde298907b3670fb83f.png,
                                 * /app/magento2/pub/media/custom_options/quote/N/a/a38967c57daaafde298907b3670fb83f.png,
                                 * 22130,
                                 * 107,
                                 * 59,
                                 * a38967c57daaafde2989
                                 * ]
                                 */
                                custom_info += `Uploaded file: https://www.victorytailgate.com/media/${vals[3]}`
                                nextOption()
                            } else {
                                // get real option value
                                async.waterfall([
                                    (go) => {
                                        var url = base_url + `/V1/products/${mageitem.sku}/options/${option.option_id}`
                                        var reqdata = {
                                            method: 'GET',
                                            url: url,
                                            headers: {
                                                'cache-control': 'no-cache',
                                                authorization: 'Bearer ' + token
                                            }, json: true
                                        }
                                        //console.log(url)
                                        request(reqdata, function (error, response, body) {
                                            if (error) console.error(error)
                                            if(response.statusCode != 200) {
                                                console.error(`option not found?`)
                                                custom_info += `<br>field ${okey + 1}: `
                                            } else {
                                                custom_info += `<br>${body.title}: `
                                            }

                                            if (body.values != undefined) {
                                                for (var val of body.values) {
                                                    if (option.option_value == val.option_type_id) {
                                                        custom_info += val.title
                                                        break
                                                    }
                                                }
                                            } else {
                                                custom_info += option.option_value
                                            }

                                            go()
                                        })
                                    }, (go) => {
                                        //console.log('next opt')
                                        go()
                                    }
                                ], (err) => {
                                    if(err) console.error(`err1: ${err}`)
                                    nextOption()
                                })
                            }
                        }, (err) => {
                            if(err) console.error(`err2: ${err}`)
                            insertStep(null, mageitem)
                        })
                    } else {
                        insertStep(null, mageitem)
                    }
                } else {
                    insertStep(null, mageitem)
                }
            })
        }, (mageitem, done) => {
            // get product data to check product type...
            magento.getProduct(token, base_url, mageitem.sku, (product) => {
                /**
                 * get slots
                 * $this->db->where("product_id",$id);
                 * $slots = $this->db->get("products_slots",$quantity,array("slot_id","slot_num"));
                 */
                var slots = []
                var slot_ids = []
                connection.query(`select slot_id, slot_num, quantity from products_slots where product_id = ? limit ?`, [mageitem.sku, mageitem.qty_ordered], (err, rows, field) => {
                    if (err) console.error(err)

                    if (rows) {
                        for (var row of rows) {
                            for(var x = 0; x < row['quantity']; x++) {
                                slots.push(row['slot_num'])
                                slot_ids.push(row['slot_id'])
                            }
                        }
                    }

                    var info = ''
                    var board = false
                    for (var attr of product.custom_attributes) {
                        if (attr.attribute_code == 'product_type'
                            && (attr.value == 1346
                                || attr.value == 1582
                                || attr.value == 1579
                                || attr.value == 1349 // unfin
                                || attr.value == 1350
                                || attr.value == 1347
                                || attr.value == 1348 // primered
                                || attr.value == 1387 // custom
                                // use parent prod 1623 ??
                            )) {
                            // board types
                            //info = 'ONE BOARD'
                            board = true
                        }
                    }

                    /**
                     * loop for splitting
                     */
                    var loopobj = []
                    for (var i = 0; i < mageitem.qty_ordered; i++)
                        loopobj.push('loop')
                    var slot = null
                    async.eachOfSeries(loopobj, (v, i, cb) => {
                        if(board)
                            console.log(`board ${(i % 2) + 1}`)

                        //if(!board || (board && (i % 2 == 0)))
                        slot = slots.length > 0 ? slots.shift() : ''

                        var thisinfo = ''
                        if (board && !slot) {
                            // ALTERNATE A/B
                            thisinfo += (i % 2 == 0) ? 'ONE BOARD PRINT A' : 'ONE BOARD PRINT B'
                        }

                        if(board && slot)
                            console.log(`in-stock board, ${slots.length} slots`.cyan)

                        if(custom_info) {
                            thisinfo += `${custom_info}<br>`
                        }

                        var values = [
                            item.vto,
                            mageitem.sku,
                            item.name,
                            '', // style
                            mageitem.price,
                            mageitem.created_at,
                            1,
                            slot,
                            0, // todo: category needed?
                            Math.round(mageitem.amount_refunded / mageitem.qty_ordered * 100) / 100,
                            custom ? 19 : mageitem.item_status,
                            thisinfo,
                            Math.round(mageitem.tax_amount / mageitem.qty_ordered * 100) / 100,
                            Math.round(mageitem.discount_amount / mageitem.qty_ordered * 100) / 100,
                            item.gid, //mageitem.uuid,
                            mageitem.sku
                        ]

                        if(mageitem.sku == 'giftcard') {
                            // use vt generic gift card sku
                            values[1] = values[15] = 22229
                            values[10] = 7 // mark gift card shipped
                        }

                        var sql = `insert into ordered_items (order_id, product_id, name, style, price, date_added, quantity, slot_nums, category, refund_amt, updated_date, cwg_customer_price, updated_by, item_status, product_type, info, tax, discount, group_id) select ?,?,?,?,?,?,?,?,?,?,now(),0,523,?,product_type,?,?,?,? from products where id=?`

                        // try insert a few times
                        var tries = 0
                        var success = false
                        async.whilst(
                            () => { return tries < 3 && success == false },
                            (retry) => {
                                tries++
                                if(tries > 1)
                                    console.log(`trying insert(${tries})...`)

                                connection.query(sql, values, (err, result) => {
                                    if (err) {
                                        console.error(`error, retrying: ${err.message}`)
                                        retry(null, tries)
                                    } else {
                                        success = true
                                        var vtitemid = result.insertId

                                        /**
                                         * status history
                                         */
                                        connection.query(`insert into ordered_item_status_history (item_id, original_status, new_status, updated_at, updated_by, notes) values (?, null, ?, now(), ?, ?)`, [vtitemid, mageitem.item_status, 523, 'Initial sync'], (err) => {
                                            if(err) console.log('status history error:'.red, err)
                                        })

                                        connection.query(`insert into vtorderitem_mageorderitem (sku, mage_site, vt_orderid, vt_itemid, mage_itemid, mage_orderid) values ?`, [[[mageitem.sku, site, item.vto, result.insertId, mageitem.item_id, mageitem.order_id]]], (err) => {
                                            if (err) {
                                                console.error(err, values)
                                                cb('link item error')
                                            } else {
                                                console.log(`\tok: ${vtitemid}`.green)

                                                /**
                                                 * REMOVE SLOTS / STOCK
                                                 */
                                                if (slot && mageitem.order_status != 4 && mageitem.item_status != 8) {
                                                    connection.query(`update products_slots set quantity = quantity - 1 where slot_num = ? and product_id = ? and quantity > 0 limit 1;`, [slot, mageitem.sku], (err) => {
                                                        if (err) console.error(err)
                                                        else console.info('slot ' + slot + ' decremented for ' + mageitem.sku)
                                                        cb()
                                                    })
                                                } else cb()
                                            }
                                        })
                                    }
                                })
                            }, (err, n) => {
                                cb(`tried 3 times, aborting`)
                                sendAlert(`Insert item failed on ${mageitem.order_id} / ${item.vto}`)
                            }
                        )
                    }, (err) => {
                        if(err) console.error(err)
                        callback(itemids)
                        done()

                        /**
                         * REMOVE SLOTS / STOCK
                         if (item.order_status != 4 && item.item_status != 8) {
                            if (slot_ids.length > 0) {
                                connection.query(`delete from products_slots where slot_id in (?) limit ?`, [slot_ids.join(','), slot_ids.length], (err, result) => {
                                    if (err) console.error(err)
                                    else console.info('slots removed for ' + item.sku)
                                })
                            }

                            })
                         }*/
                        connection.query(`update products set stock = stock - ? where id = ? and stock > 0 limit 1`, [mageitem.qty_ordered, mageitem.sku], (err, result) => {
                            //if (err) console.error(err)
                        })
                    })
                })
            })
        }
    ], (err) => {
        if(err) console.error(err)
    })
}

const updateTransactions = (order, vto, connection, connection_mage, callback) => {
    //console.log('update transactions...')
    async.waterfall([
        (nextCheck) => {
            /**
             * TRANSACTIONS
             */
                //var sql = `select * from sales_payment_transaction where order_id = ?`
            var sql = `select
              t.txn_type as type,
              p.method,
              if(t.txn_type = 'refund', p.amount_refunded, p.amount_paid) as amount,
              t.txn_id as payment_id,
              t.transaction_id as magento_transaction_id,
              t.created_at as date_added
            from sales_order_payment p
              left join sales_payment_transaction t on t.payment_id = p.entity_id
            where t.order_id = ?`
            var q = connection_mage.query(sql, [order.entity_id], (err, rows) => {
                //console.log(rows)
                //console.log(q.sql)
                if(rows.length > 0) {
                    /**
                     sample gateway response transaction table:
                     {"raw_details_info":{"response_code":1,"response_subcode":"","response_reason_code":0,"response_reason_text":"","approval_code":"182103","auth_code":"182103","avs_result_code":"Y","transaction_id":"61192020689","reference_transaction_id":"","invoice_number":"000115928","description":"","amount":"234.98","method":"CC","transaction_type":"auth_capture","customer_id":"","md5_hash":"BF45D66ACE3E78E8F7163867CF060959","card_code_response_code":"M","cavv_response_code":"","acc_number":"XXXX3565","card_type":"Visa","split_tender_id":"","requested_amount":"","balance_on_card":"","profile_id":"1928601037","payment_id":"1941714219","is_fraud":false,"is_error":false}}
                     */
                    /**
                     * payment table authnet
                     * {"save":0,"acceptjs_key":null,"acceptjs_value":null,"method_title":"Credit Card Payment","response_code":1,"response_subcode":"","response_reason_code":0,"response_reason_text":"","approval_code":"202588","auth_code":"202588","avs_result_code":"N","transaction_id":"61196305453","reference_transaction_id":"","invoice_number":"000117096","description":"","amount":"254.98","method":"CC","transaction_type":"auth_capture","customer_id":"63462","md5_hash":"0C510A286C0332DB8DE4F97A560236FC","card_code_response_code":"M","cavv_response_code":"","acc_number":"XXXX1012","card_type":"AmericanExpress","split_tender_id":"","requested_amount":"","balance_on_card":"","profile_id":"1928870073","payment_id":"1942004944","is_fraud":false,"is_error":false}
                     */
                    /**
                     * payment table paypal
                     * {"paypal_express_checkout_shipping_overridden":1,"paypal_express_checkout_shipping_method":"","paypal_payer_id":"4LYL7UFN5UJQ6","paypal_payer_email":"dwfloridaartist@gmail.com","paypal_payer_status":"unverified","paypal_address_status":"Confirmed","paypal_correlation_id":"c8988982c3b63","paypal_express_checkout_payer_id":"4LYL7UFN5UJQ6","paypal_express_checkout_token":"EC-4MB80560NU1646718","method_title":"PayPal Express Checkout","paypal_express_checkout_redirect_required":null,"paypal_protection_eligibility":"Eligible","paypal_payment_status":"completed","paypal_pending_reason":"None"}
                     */
                    var values = rows.map(x => {
                        //var info = x.additional_information != null && x.additional_information != 'null' ? JSON.parse(x.additional_information).raw_details_info : null
                        //console.log(info, x.additional_information)
                        return [
                            vto,
                            x.type,
                            x.method,
                            x.amount,
                            x.payment_id,
                            x.magento_transaction_id,
                            x.date_added
                        ]
                    })
                    console.log('transactions...')
                    async.eachSeries(values, (value, nextInsert) => {
                        var q = connection.query(`insert into transactions (order_id, type, method, amount, payment_id, magento_transaction_id, date_added) values (?)`, [value], (err) => {
                            //console.log(q.sql, err)
                            if (err) {
                                if(err.code != 'ER_DUP_ENTRY') console.error(err.sqlMessage.red)
                            } else console.log(`\t${value[1]} inserted`.green)
                            nextInsert()
                        })
                    }, nextCheck)
                } else nextCheck()
            })
        }, (nextCheck) => {
            /**
             * CHECK STORE CREDIT
             */
            if(typeof order.extension_attributes.customer_balance_amount != 'undefined') {
                var amt = order.extension_attributes.customer_balance_amount
                var sql = `insert into transactions (order_id, type, method, amount, payment_id, date_added) values ?`
                //var date = new Date().toISOString().slice(0, 19).replace('T', ' ')
                var values = [[
                    vto,
                    'customer balance',
                    'store credit',
                    amt,
                    `SC-${order.quote_id}`,
                    order.created_at
                ]]
                connection.query(sql, [values], (err) => {
                    if (err && err.code != 'ER_DUP_ENTRY') console.error(err.sqlMessage.red)
                    nextCheck()
                })
            } else {
                nextCheck()
            }
        }, (nextCheck) => {
            /**
             * CHECK GIFT CARD
             */
            if (order.extension_attributes.gift_cards.length > 0) {
                var sql = `insert into transactions (order_id, type, method, amount, payment_id, date_added, note) values ?`
                var values = order.extension_attributes.gift_cards.map(x => {
                    return [
                        vto,
                        'gift card',
                        'gift card',
                        x.amount,
                        `GC-${order.quote_id}-${x.code}`,
                        order.created_at,
                        x.code
                    ]
                })
                connection.query(sql, [values], (err) => {
                    if (err && err.code != 'ER_DUP_ENTRY') console.error(err.sqlMessage.red)
                    nextCheck()
                })
            } else nextCheck()
        }
    ], callback)
}

// Express Stuff
const app = express()

var processed = []
var cur_cust = []
var cur_prod = []

var port = process.env.PORT || 3100
var site = process.argv[2] || 'none'
var site_ids
var base_url

//attach jquery to express
app.get('/jquery.min.js', (req, res) => {
    res.sendFile(__dirname + '/node_modules/jquery/dist/jquery.min.js')
})

app.get('/sync/orders-legacy/:site', (req, res) => {
    cur_cust = []
    processed = []
    cur_prod = []

    site = req.params.site
    var token = config[`magento_api_Access_Token_${site}`]
    var store_id = config[`store_id_${site}`]
    base_url = config[`magento_base_url_${site}`]
    site_ids = config[`sites_${site}`]
    var qs = url.parse(req.url, true).query
    var oid = qs.orderid || false
    syncOrdersLegacy(token, base_url, store_id, site_ids, oid, site, null);

    res.end("legacy order sync called...\n")
})

app.get('/tracking/:site', (req, res) => {
    site = req.params.site
    var token = config[`magento_api_Access_Token_${site}`]
    var qs = url.parse(req.url, true).query
//if(qs.orderid)
    addTracking(token, site, qs.orderid, qs.dry, null)

    res.end()
})

app.use(express.static('public'))

// Startup Stuff
async.waterfall([(cb) => {
    port = 3100

    app.listen(port, "0.0.0.0", cb)
}], err => {
    if(err){
        console.log('Startup failed: ' + err)
        return
    }
    moment.tz.setDefault('America/New_York')
    var msg = 'Startup successful! ('+port+') '+"\n"+ moment().format()
    console.log(msg.white)

    base_url = config[`magento_base_url_${site}`]
    var token = config[`magento_api_Access_Token_${site}`]
    /*var qs = url.parse(req.url, true).query*/
    site_ids = config[`sites_${site}`]

    if(site!='none') {
      syncOrdersLegacy(token, base_url, config[`store_id_${site}`], site_ids, false, site, syncOrdersLegacy)
    }
})
